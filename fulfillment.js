	'use strict';


const dialogflowFulfillment = require('../dialogflow-fulfillment-builder');

const config = {
    'platformsEnabled': ['TEXT'] // define the platforms to be supported as per requirement
};

//const dialogflow = require('@google-cloud/dialogflow');

var dialogConfig = {};
var initialized = false;
var callableFuncions = [];

function setCallableFuncions(o) {
	console.log('*** setCallableFuncions: ' + JSON.stringify(o));
	
	callableFuncions = o;
}

async function initialize(projectId) {
	if (initialized == false) {
		console.log('### INITIALIZED ###');
		//listIntents(projectId);
	}
	initialized = true;
}

function setLanguage(fulfillment, lang) {
	setGlobalParameter(fulfillment, 'language', lang);
}

function getLanguage(fulfillment) {
	if (getGlobalParameter(fulfillment, 'language')) {		
		const lang = getGlobalParameter(fulfillment, 'language');
		console.log('*** in getLanguage(); previously set language is: ' + lang);
		return lang;
	}
	if (fulfillment._request && fulfillment._request.queryResult && fulfillment._request.queryResult.languageCode) {
		const lang = fulfillment._request.queryResult.languageCode;
		console.log('*** in getLanguage(); language from fufillment is: ' + lang);
		return lang;
	}
	console.log('*** in getLanguage(); returning default ');
	return 'en';
}


// work in progress; not currently used
// for internal testing only
async function listIntents(projectId) {
	try {
	  // Construct request
	  const intentsClient = new dialogflow.IntentsClient();
	  // The path to identify the agent that owns the intents.
	  const projectAgentPath = intentsClient.agentPath(projectId);

	  console.log(projectAgentPath);

	  const request = {
		parent: projectAgentPath,
		};

	  // Send the request for listing intents.
	  const [response] = await intentsClient.listIntents(request);
	  var updateIntentRequest;
	  response.forEach(intent => {
		console.log('====================');
		
		console.log(`Intent display name: ${intent.displayName}`);
		console.log('Entire intent: ' + JSON.stringify(intent));
		
		
		
		if (intent.displayName == 'lookup') {
			lookupIntent = intent;
			lookupIntent.events = ["_LOOKUP"];
			updateIntentRequest = {
				lookupIntent,
				languageCode: "en-US"
			};
			
		}
	  });
	  const result = await intentsClient.updateIntent(updateIntentRequest);
	} catch (e) {
		console.log('*** ERROR: ' + e);
	}
}






function setDialogConfig(config) {
	dialogConfig = config;
}

function addContainerIntentToConfig(intentConfig) {
	if (!dialogConfig.intents) {
		dialogConfig.intents = {};
	}
	dialogConfig.intents[intentConfig.name] = intentConfig;
	return dialogConfig;
}

function createDefaultsFromPayload(fulfillment) {
	console.log('*** entering createDefaultsFromPayload; fulfillment = ' + JSON.stringify(fulfillment));
	if (fulfillment && fulfillment._request && fulfillment._request.queryResult && fulfillment._request.queryResult.fulfillmentMessages && fulfillment._request.queryResult.fulfillmentMessages[1]) {
		
		const payload = fulfillment._request.queryResult.fulfillmentMessages[1].payload;
		console.log('*** createDefaultsFromPayload; found payload: ' + JSON.stringify(payload));
		addDefaultPropertiesToConfig(payload);
		fulfillment.setOutputContext('default_properties', 50, payload);
		console.log('***  created defaults From Payload; fulfillment = ' + JSON.stringify(fulfillment));
	}
		
}

function setDefaultsFromPayload(fulfillment) {
	if (fulfillment) {	
		if (fulfillment.getContext('default_properties')) {
			const existingDefaults = fulfillment.getContext('default_properties').parameters;
			
			
			dialogConfig.defaults = existingDefaults;
		}
	}
}

function addDefaultPropertiesToConfig(defaults) {
	
	dialogConfig.defaults = defaults;
	
	if (!dialogConfig.intents) {
		dialogConfig.intents = {};
	}
	
	if (!dialogConfig.simpleIntents) {
		dialogConfig.simpleIntents = {};
	}
	return dialogConfig;
}

function addSimpleIntentToConfig(intentConfig) {
	if (!dialogConfig.simpleIntents) {
		dialogConfig.simpleIntents = {};
	}
	dialogConfig.simpleIntents[intentConfig.intentName] = intentConfig;
	return dialogConfig;
}

function getSimpleIntentConfig(intentName) {
	if (intentName == null) {
		return null;
	}
	if (dialogConfig && dialogConfig.simpleIntents) {
		const intentConfig = dialogConfig.simpleIntents[intentName];
		if (!intentConfig) {
			return null;
		}
		return intentConfig;
	}
	throw new Error('no simple intent config exists when  getting config for ' + intentName);
}

function getContainerIntentConfig(containerIntentName) {
	if (dialogConfig && dialogConfig.intents) {
		const intentConfig = dialogConfig.intents[containerIntentName];
		if (!intentConfig) {
			return null;
		}
		return intentConfig;
	}
	throw new Error('no intent configs exist when  getting config for ' + containerIntentName);
}

function isChildIntent(intentName) {
	if (dialogConfig && dialogConfig.intents) {
		for (const [intent, intentConfig] of Object.entries(dialogConfig.intents)) {
			const params = intentConfig.requiredParameters;
			if (params) {
				for (var i = 0; i < params.length; i++) {
					const param = params[i];
					if (param.intentName == intentName) {
						console.log(`found ${intentName}: it is a child of ${intent}`);
						return true;
					}
				}
			}
		}
	}
	return false;
}

function isContainerIntentEvent(intentName) {
	if (dialogConfig && dialogConfig.intents) {
		for (const [intent, intentConfig] of Object.entries(dialogConfig.intents)) {
			
			if (intentToEventName(intent) == intentName) {
				console.log(` ${intentName} is an event for top-level intent`);
				return true;
			}
			
		}
	}
	return false;
}

function isSimpleIntentEvent(intentName) {
	if (dialogConfig && dialogConfig.simpleIntents) {
		for (const [intent, intentConfig] of Object.entries(dialogConfig.simpleIntents)) {
			
			if (intentToEventName(intent) == intentName) {
				console.log(` ${intentName} is an event for simple intent`);
				return true;
			}
			
		}
	}
	return false;
}

function isChildIntentEvent(intentName) {
	if (dialogConfig && dialogConfig.intents) {
		for (const [intent, intentConfig] of Object.entries(dialogConfig.intents)) {
			const params = intentConfig.requiredParameters;
			if (params) {
				for (var i = 0; i < params.length; i++) {
					const param = params[i];
					if (intentToEventName(param.intentName) == intentName) {
						console.log(` ${intentName} is an event for a child intent of ${intent}`);
						return true;
					}
				}
			}
		}
	}
	return false;
}

function getGlobalIntentConfig(globalIntentName) {
	if (isSimpleIntent(globalIntentName)) {
		return getSimpleIntentConfig(globalIntentName);
	}
	
	if (isContainerIntent(globalIntentName)) {
		return getContainerIntentConfig(globalIntentName);
	}
	throw new Error('no intent configs exist for global: ' + globalIntentName);
}

function getStartIntentName(fulfillment, containerIntentName) {
	
	console.log('*** Entering getStartIntentName: fulfillment = ' + JSON.stringify(fulfillment) + '; containerIntentName = ' + containerIntentName);

	const intentConfig = getContainerIntentConfig(containerIntentName);
	
	if (!intentConfig) {
		throw Error(`getStartIntentName(): config for ${containerIntentName} does not exist`);
	}
	
	var startIntent = resolve(fulfillment, intentConfig.startIntent);
	
	if (startIntent == 'INTENT_COMPLETE') {
		goToNextForContainerIntent(fulfillment);
		return;
	}
	
	// if startIntent has not been explicitly specified, start with the first required parameter
	if (!startIntent) {
		const params = intentConfig.requiredParameters;	
		if (params && params.length > 0) {
			startIntent = params[0].intentName;
		}
	}
	
	return startIntent;
}

function setBargeIn(fulfillment, value) {
	console.log(`**** in setBargeIn: setting barge-in to ${value}`);
	fulfillment._response.fulfillmentMessages.push({ "payload": { "barge-in": value },
													"platform":"PLATFORM_UNSPECIFIED"});
	console.log('**** in setBargeIn: fulfillment = ' + JSON.stringify(fulfillment));
}

function goToNextForContainerIntent(fulfillment) {
	const nextIntentName = getNextForTopLevelIntent(fulfillment, getParentIntentName(fulfillment));
	console.log(`**** in goToNextForContainerIntent; next intent is ${nextIntentName}`);	
	
		
	if (!nextIntentName || nextIntentName == 'end_conversation') { 
		// no next intent -- throw END_CONVERSATION
		console.log('*** No next for  top level intent -- about to end conversation');
		endConversation(fulfillment);
		return;
	}
	console.log(`**** in goToNextForContainerIntent; about to trigger ${nextIntentName}`);
	triggerIntent(fulfillment, nextIntentName);
}

function goToNextForSimpleIntent(fulfillment, intentName) {
	const nextIntentName = getNextIntentName(fulfillment, null, intentName);
	console.log(`**** in goToNextForSimpleIntent; next intent is ${nextIntentName}`);	
	
		
	if (!nextIntentName || nextIntentName == 'end_conversation') { 
		// no next intent -- throw END_CONVERSATION
		console.log(`*** No next for  simple intent ${intentName} -- about to end conversation`);
		endConversation(fulfillment);
		return;
	}
	console.log(`**** in goToNextForSimpleIntent; about to trigger ${nextIntentName}`);
	triggerIntent(fulfillment, nextIntentName);
}


function handleContainerIntent(fulfillment, containerIntentName, intentName) {
	
	try {
	
	console.log('*** Updated: Entering handleContainerIntent: fulfillment = ' + JSON.stringify(fulfillment) + '; containerIntentName = ' +containerIntentName + '; intentName: ' + intentName);
	if (!intentName) {
		setParentIntentName(fulfillment, containerIntentName);
		if (!isGlobalIntent(containerIntentName)) {
			saveCurrentIntentName(fulfillment, containerIntentName);
			// save the name of the intent that got matched (unless it's a global command)
	  }
	}
	const intentConfig = getContainerIntentConfig(containerIntentName);
	
	if (!intentConfig) {
		throw new Error(`**** ERROR: No config found for ${containerIntentName}`);
	}
	if (!intentName) { // intent is kicked off by an event, 
						// but not because a required param was filled
	    console.log('*** in handleContainerIntent: caller phrase triggered the intent (or it was kicked off by event)');
		
		
		if (intentConfig.prompt) {
			fulfillment.setResponseText(expandParametersInPromptText(fulfillment, intentConfig.prompt));
			console.log('*** in handleContainerIntent: added prompt: ' + intentConfig.prompt);
		} else {
			//fulfillment.setResponseText(' ');
			console.log(`*** in handleContainerIntent: ${containerIntentName} does not have a prompt -- not setting fulfillment text`);
		}
		
		const intentName = getStartIntentName(fulfillment, containerIntentName);
		
			
		console.log(`*** in handleContainerIntent: found startIntent: ${intentName}`);
		
		
		if (isRequiredParameterFilled(fulfillment, containerIntentName, intentName)) {
			// if a required parameter has been filled in NLU, check if it needs to be confirmed
			console.log('*** in handleContainerIntent after NLU: required parameter for intentName ' + intentName + ' was filled');
			
			saveUnconfirmedParameters(fulfillment);
			console.log(`++++ calling resolve on "confirm" for top-level intent ${containerIntentName} and child intent ${intentName}`);
			const confirmType = getConfirm(fulfillment, containerIntentName, intentName);
			if (confirmType == true) {
				console.log('*** in handleContainerIntent: confirm is true; aboout to trigger confirmation');
				triggerConfirmation(fulfillment, intentName, 'initial');
				return;
			} else {
				copyFilledParameters(fulfillment);
				clearUnconfirmedParameters(fulfillment);				
				console.log('*** in handleContainerIntent after NLU: confirm is false; aboout to trigger next intent');
				console.log(`++++ calling resolve on "next" for top-level intent ${containerIntentName} and child intent ${intentName}`);
				var nextIntent = resolve(fulfillment, getNextIntentName(fulfillment, containerIntentName, intentName));
				if (!nextIntent) {
					console.log(`*** in handleContainerIntent: next for startIntent ${intentName} is not specfied `);
					
				
					nextIntent = getDefaultNextIntentName(fulfillment, containerIntentName,
								getRequiredParameterName(fulfillment, containerIntentName, intentName));
					console.log(`*** in handleContainerIntent: default nextIntent for startIntent ${intentName} is ${nextIntent} `);
				}
				if (nextIntent && nextIntent != 'INTENT_COMPLETE') {
					console.log(`*** in handleContainerIntent:  nextIntent for startIntent ${intentName} exists; triggering `);
					triggerIntent(fulfillment, nextIntent);
					return;
				} else {
					// the start intent must have been the only child intent;
					// it has been filled -- so the top level intent is done. 
					// Let's find and process the next top level intent
					goToNextForContainerIntent(fulfillment);
					return;
				}			
				
				
			}
		} else {
			console.log(`*** in handleContainerIntent for ${containerIntentName} after NLU: required parameter for start intent ${intentName} was NOT filled; about to trigger intent`);
			triggerIntent(fulfillment, intentName);
			return;
		}
		
		
	}
	
	if (intentName) {  
		// we just completed filling one of the required params
		// the name of the filled parameter was stored in 'param' parameter
		var nextIntentName = getNextIntentName(fulfillment, containerIntentName, intentName);
		
		console.log(`*** In handleContainerIntent or ${containerIntentName}:  ${intentName} filled required parameter for ${containerIntentName}; next intent resolved to: ${nextIntentName}`);
		if (!nextIntentName) {

			nextIntentName = getDefaultNextIntentName(fulfillment, containerIntentName, getRequiredParameterName(fulfillment, containerIntentName, intentName));
			console.log(`*** In handleContainerIntent for ${containerIntentName}: ${intentName} filled required parameter; next intent is not specified -- got default next intent: ${nextIntentName}`);
		}
		
		
		
		if (!nextIntentName || 'INTENT_COMPLETE' == nextIntentName) {  // INTENT_COMPLETE  means 
																		// that all the work for top-level-intent is done
			// filled all the child intents -- all done with this top level intent
            console.log(`**** nextIntentName is null or INTENT_COMPLETE -- done filling required params`);			
			goToNextForContainerIntent(fulfillment);
			return;
		}
		
		console.log(`*** about to trigger next intent for ${intentName}, which is ${nextIntentName} `);
		triggerIntent(fulfillment, nextIntentName, {mode:'initial'});
	}	
	
	} catch (e) {
		console.log('%%%%% Caught error in handleContainerIntent: ' + e);
	}
	
}

/**
function endConversation(fulfillment) {
  var diagnosticInfo = fulfillment._request.queryResult.diagnosticInfo;
  if (diagnosticInfo) {
	diagnosticInfo.end_conversation = true;
  } else {
	fulfillment._request.queryResult.diagnosticInfo = {end_conversation:true};
  }
  var original = fulfillment._request.originalDetectIntentRequest;
  if (original) {
	  var payload = original.payload;
	  if (payload) {
		  original.payload.expectUserResponse=false;
		  original.payload.end_conversation=true;
	  } else {
		  original.payload = {expectUserResponse:false, end_conversation:true};
	  }
  }
  console.log('**** Called endConversation; fulfillment: ' + JSON.stringify(fulfillment));
}
*/

function enableOtherActiveIntents(fulfillment, intentName) {
	const intents = getActiveIntents(fulfillment, intentName);
	console.log("*** In enableOtherActiveIntents for " + intentName + "; found active intents: " + JSON.stringify(intents));
	if (intents) {
		for (var i = 0; i < intents.length; i++) {
			enableIntent(fulfillment, intents[i]);
		}
	}
}

function getDefaultNextIntentName(fulfillment, parentIntentName, paramName) {
	console.log('*** Entering getDefaultNextIntentName; paramName: ' + paramName);
	try {
		var nextIntent = '';
		if (!parentIntentName || parentIntentName == '') {
			console.log (`**** WARNING: no stored parent intent found when in getDefaultNextIntentName for ${paramName}`);
			return null;
		}
		const intentConfig = getContainerIntentConfig(parentIntentName);
		if (!intentConfig) {
			console.log (`**** WARNING:  getDefaultNextIntentName: no config  found for parent intent ${parentIntentName}`);
			return null;
		}
		
		if (!intentConfig.requiredParameters) {
			console.log (`**** WARNING:  getDefaultNextIntentName: no required params  found for parent intent ${parentIntentName}`);
			return null;
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.name == paramName) {
				console.log('*** In getDefaultNextIntentName; found  paramName = ' + paramName + ' at i = ' + i);
				if ((i + 1) < intentConfig.requiredParameters.length) {
					const nextParamConfig = intentConfig.requiredParameters[i + 1];
					console.log('*** In getDefaultNextIntentName; ' + paramName + ' is not the last required paramerer; nextParamConfig = ' + JSON.stringify(nextParamConfig));
					return nextParamConfig.intentName;
				}
			}
		}
		return null;
	} catch (e) {
		console.log('%%%%ERROR: Caught error in getDefaultNextIntentName: ' + e);
		return null;
	}
	
}

function logDebugInfoStart(fulfillment) {
    console.log('*** entering ' + JSON.stringify(fulfillment._request.queryResult.intent.displayName));
}

function logDebugInfoEnd(fulfillment) {
    console.log('*** Exiting ' + JSON.stringify(fulfillment._request.queryResult.intent.displayName));
}



function enableTopLevelIntents(fulfillment) { // TODO: update to allow enabling select intents only
    console.log('*** Entering enableTopLevelIntents: fulfillment = ' + JSON.stringify(fulfillment));
	for (const [intent, value] of Object.entries(dialogConfig.intents)) {
		if (intent != 'NLU' && intent != 'default_welcome_intent' && !isGlobalIntent(intent)) {
			console.log('enabling intent: ' + intent);
			enableIntent(fulfillment, intent);
		}
	}
}

function getGlobalsForIntent(fulfillment, intentName) {
	if (intentName == 'NLU') {
		console.log(`*** getting globals for NLU`); 
		const intentConfig = getContainerIntentConfig(intentName);
		if (intentConfig) {
			console.log('*** found globals for NLU: ' + JSON.stringify(intentConfig.globals));
			return intentConfig.globals;
		}
		return null;
	}
	var parentIntentName = null;
	if (isContainerIntent(intentName)) {
		parentIntentName = getParentIntentName(fulfillment);
	}
	console.log(`*** getting globals for parent ${parentIntentName} and child ${intentName}`);
	const globals = getGlobals(fulfillment, parentIntentName, intentName);
	console.log(`*** found globals:  ${globals}`);
	if (!globals) {
		return getDefaultGlobals();
	}
	return globals;
}

function enableGlobalIntents(fulfillment, intentName) { 
    console.log('*** Entering enableGlobalIntents for intent:  ' + intentName);
	const globals = getGlobalsForIntent(fulfillment, intentName);
	
	if (globals) {
		console.log(`*** globals for ${intentName} exist; enabling`);
		for (var i = 0; i < globals.length; i++) {
			
			console.log(`enabling ${intentName}-specific global: ${globals[i]}`);
			enableIntent(fulfillment, globals[i]);
		}
	} else {
		if (getGlobalIntentNames()) {
			console.log(`*** globals for ${intentName} do not exist; enabling default globals`);
			for (var i = 0; i < getGlobalIntentNames().length; i++) {
					let intent = getGlobalIntentNames()[i];
					console.log('enabling global intent: ' + intent);
					enableIntent(fulfillment, intent);
				
			}
		
		}
	}
}

function getContext(fulfillment, name) {
	const sessionID = fulfillment._request.session;
	var fullName = sessionID + '/contexts/' + name;
	console.log('*** full context name: ' + fullName);
	var allContexts = fulfillment.getAllOutputContexts();
	for (var i = 0; allContexts && (i < allContexts.length); i++) {
		var thisContext = allContexts[i];
		//console.log('*** this context ' + JSON.stringify(thisContext));
		if (thisContext.name == fullName) {
			return thisContext;
		}
	}
    return null;
}



function setFulfillmentTextFromBuffer(fulfillment) {
	const context = fulfillment.getContext('fulfillment_text_buffer');
	console.log('*** setFulfillmentTextFromBuffer: found buffer context: ' + JSON.stringify(context));
	if (context != null) {
		const cachedText = context.parameters.buffer;
		if (!(cachedText == '' || cachedText == ' ')) {
			console.log('*** cached text is not empty -- setting Response text ');
			fulfillment.setResponseText(expandParametersInPromptText(fulfillment, cachedText));
		}
	}
}

function setEvent(fulfillment, name, lang, parameters) {
	console.log('*** Entering setEvent: name = ' + name + '; lang = ' + lang + '; params = ' + JSON.stringify(parameters) + '; fulfillment = ' + JSON.stringify(fulfillment));
	
	const intentDisplayName = getIntentName(fulfillment);
	if (noParametersToFill(fulfillment, intentDisplayName)) {
		console.log('*** Intent ' + intentDisplayName + ' is a prompt-only intent; buffering fulfillment messages');
		bufferFulfillmentText(fulfillment);
	}
	fulfillment.setEvent(name, lang || getLanguage(fulfillment), parameters || {});
}


function getFulfillmentMessages(fulfillment) {
	console.log('*** In getFulfillmentMessages; fulfillment: ' + JSON.stringify(fulfillment));
	var returnText = '';
	const messages = fulfillment._response.fulfillmentMessages;
	for (let i = 0; i < messages.length; i++) {
            if (messages[i].text && messages[i].text.text && messages[i].text.text.length > 0) {
				for (let n = 0; n < messages[i].text.text.length; n++) {
					const text = messages[i].text.text[n];
					returnText = returnText + text;
				}
               
            }
    }
	console.log('*** In getFulfillmentMessages; got text messages: ' + returnText);
	return returnText;
}

function bufferFulfillmentText(fulfillment) {
	const fulfillmentText = getFulfillmentMessages(fulfillment);
	console.log('*** In bufferFulfillmentText; buffering text: ' + fulfillmentText);
	if (fulfillmentText != null) {
		fulfillment.setOutputContext('fulfillment_text_buffer', 1, {'buffer': fulfillmentText});
	}
}
function resolve(fulfillment, value) {
	if (!value) {
		return null;
	}
	
	if (value instanceof Function) {
		console.log('***** In resolve: value is a Function: ' + value.name);
		return value(fulfillment);
	}
	
	
	console.log('***** In resolve: value is not a function:  ' + value);
	return value;
	
}

function isKnownIntentEvent(query) {
	return (isChildIntentEvent(query) || isContainerIntentEvent(query) || isSimpleIntentEvent(query) || query == 'NLU'
	        || query == 'Welcome' || query == 'WELCOME' || query == 'END_CONVERSATION' || query == 'DEFAULT_ERROR_INTENT'
			|| query == 'CONFIRM'); 
	
}

function isDefaultIntentName(name) {
	return name.includes('_');
}

function isEventCaught(fulfillment) {
	const query = fulfillment._request.queryResult.queryText;
	if (query) {
		const intentName = getIntentName(fulfillment);
		const result = ((query.includes('_') || query == intentName.toUpperCase() || isKnownIntentEvent(query)) && query === query.toUpperCase());
		console.log('*** isEventCaught for: ' + query + ' is: ' + result + '; fulfillment = ' + JSON.stringify(fulfillment));
		return result;
	}
	return false;
}

function getEventName(fulfillment) {
	const query = fulfillment._request.queryResult.queryText;
	if (isEventCaught(fulfillment)) {
		return query;
	}
	
	return null;
}

function addResponseText(fulfillment, moreText) {
	
	
	var fulfillmentText = getDefaultFulfillmentText(fulfillment);
	console.log('*** addResponseText: adding ' + moreText);
	console.log('*** addResponseText: found existing fulfillment text ' + fulfillmentText || '');
	if (fulfillmentText != null) {
		fulfillment.setResponseText(expandParametersInPromptText(fulfillment, fulfillmentText + ' ' + moreText));
	} else {
		fulfillment.setResponseText(expandParametersInPromptText(fulfillment, moreText));
	}
	console.log('*** addResponseText: set combined text to be: ' + fulfillment._response.fulfillmentText || '');
}

function addDefaultResponseText(fulfillment) {
	try {
		const defaultText = getDefaultFulfillmentText(fulfillment)
		console.log('*** addDefaultResponseText: adding ' + defaultText || '');
		addResponseText(fulfillment, defaultText || ' ');
	} catch (e) {
        console.log(e);
        console.log(e.message);
    }
}

function getDefaultFulfillmentText(fulfillment) {
	
	try {
		if (fulfillment && fulfillment._request && fulfillment._request.queryResult && fulfillment._request.queryResult.fulfillmentMessages && fulfillment._request.queryResult.fulfillmentMessages[0]) {
			const messages = fulfillment._request.queryResult.fulfillmentMessages[0];
			var text = '';
			if (messages.text && messages.text.text && messages.text.text[0]) {
				text = messages.text.text[0];				
			} 
			console.log('*** setDefaultResponseText: setting ' + text);
				
			return text;
		}
	} catch (e) {
        console.log(' *** caught error in getDefaultFulfillmentText: ' + e);
		return '';
    }
	
}

function getDefaultRetryFulfillmentText(fulfillment) {
	
	try {
		if (fulfillment && fulfillment._request && fulfillment._request.queryResult && fulfillment._request.queryResult.fulfillmentMessages && fulfillment._request.queryResult.fulfillmentMessages[1]) {
			const messages = fulfillment._request.queryResult.fulfillmentMessages[1];
			var text = '';
			if (messages.text && messages.text.text && messages.text.text[0]) {
				text = messages.text.text[0];				
			} 
			console.log('*** setDefaultResponseText: setting ' + text);
				
			return text;
		}
	} catch (e) {
        console.log(' *** caught error in getDefaultFulfillmentText: ' + e);
		return '';
    }
	
}

function setDefaultResponseText(fulfillment) {
	try {
		var text = getDefaultFulfillmentText(fulfillment);
		text = expandParametersInPromptText(fulfillment, text, getIntentName(fulfillment));
		console.log('*** setDefaultResponseText: setting ' + text);
		fulfillment.setResponseText(text);
	} catch (e) {
        console.log(e);
        console.log(e.message);
    }
}

function setDefaultRetryResponseText(fulfillment) {
	try {
		var text = getDefaultRetryFulfillmentText(fulfillment);
		if (!text) {
			text = getDefaultFulfillmentText(fulfillment);
		}
		text = expandParametersInPromptText(fulfillment, text, getIntentName(fulfillment));
		console.log('*** setDefaultRetryResponseText: setting ' + text);
		fulfillment.setResponseText(text);
	} catch (e) {
        console.log(e);
        console.log(e.message);
    }
}


function handleWelcomeIntent(fulfillment) {
    try {
		logDebugInfoStart(fulfillment);
		const greeting = getPrompt(fulfillment, getIntentName(fulfillment));
		
		
		if (greeting && greeting.length > 0) {
		
		  fulfillment.setResponseText(greeting);
		}

		console.log('**** Greeting for Welcome:' + greeting);
        //setBargeIn(fulfillement, false);

        fulfillment.setOutputContext('welcome_breadcrumb', 2, { 'Genesys-Conversation-Id': fulfillment._request.originalDetectIntentRequest.payload['Genesys-Conversation-Id'] });
		const data = {'last_intent':getIntentName(fulfillment),'Genesys-Conversation-Id':fulfillment._request.originalDetectIntentRequest.payload['Genesys-Conversation-Id'] };
		fulfillment.setOutputContext('global', 50, {data:data});
		
		const nextIntent = getNextForTopLevelIntent(fulfillment, getIntentName(fulfillment));
		
		if (!nextIntent) {
			triggerIntent(fulfillment, 'NLU'); // default to NLU as the default next intent after Welcome
		} else {
			triggerIntent(fulfillment, nextIntent);
		}
		
	}
	
    catch (e) {
        console.log(e);
        console.log(`%%%%ERROR: caught in handleWelcomeIntent: ${e.message}`);
    }
}

function handleNLU(fulfillment, mode) {
	console.log('**** Entering handleNLU; mode = ' + mode);
    try {
		if (!mode) {
			mode = 'initial';
		}
		setParentIntentName(fulfillment, 'NLU');
		fulfillment.setResponseText(getPrompt(fulfillment, 'NLU', mode));
		enableTopLevelIntents(fulfillment);
		enableGlobalIntents(fulfillment, 'NLU');
		enableOtherActiveIntents(fulfillment, 'NLU');
		//setBargeIn(fulfillment, getBargeIn());
	} catch (e) {
		console.log(e);
        console.log('Caught error in hanldeNLU: ' + e.message);
	}
}

function getParameter(fulfillment, name) {
	const value = fulfillment._request.queryResult.parameters[name];
	return value;
}

function getParentContext(fulfillment) {
	return fulfillment.getContext('parent_intent');
}

function getIntentContext(fulfillment, intentName) {
	const context = fulfillment.getContext(intentToContextName(intentName));
	if (context && context.parameters) {
		if (!context.parameters.requiredParameters) {
			context.parameters.requiredParameters = {};
			setIntentContext(fulfillment, intentName, context.parameters);
		}
		return context;
	}
	throw Error(`getIntentContext: no conext ${intentName} exists`);
}

function getFilledRequiredParameters(fulfillment, intentName) {
	
	var context = {};
	if (isSimpleIntent(intentName)) {
		context = getIntentContext(fulfillment, intentName);
	} else {
		context = getParentContext(fulfillment);
	}

	
	if (context && context.parameters && context.parameters.requiredParameters) {
		return context.parameters.requiredParameters;
	}
	return {};
}

function getFilledRequiredParameter(fulfillment, name, intentName) {
	

	const allFilledParams = getFilledRequiredParameters(fulfillment, intentName);
	if (allFilledParams) {
		return allFilledParams[name];
	}
	return null;
}

function setRequiredParameter(fulfillment, name, value, intentName) {
	
	if (!name) {
		return;
	}
	
	var context = {};
	if (isSimpleIntent(intentName)) {
		context = getIntentContext(fulfillment, intentName);
	} else {
		context = getParentContext(fulfillment);
	}
	if (context && context.parameters && context.parameters.requiredParameters) {
		context.parameters.requiredParameters[name] = value;
	}
	if (!isSimpleIntent(intentName)) {
		setParentContext(fulfillment, context.parameters);
	} else {
		setIntentContext(fulfillment, intentName, context.parameters);
	}
}

function clearRequiredParameter(fulfillment, name, intentName) {
	
	if (!name) {
		return;
	}	
	setRequiredParameter(fulfillment, name, null, intentName);
}

function copyFilledParameters(fulfillment, intentName) {
	

	var context = {};
	
	if (isSimpleIntent(intentName)) {
		context = getIntentContext(fulfillment, intentName);
	} else {
		context = getParentContext(fulfillment);
	}
	var filledParams = {};
	if (context && context.parameters && context.parameters.requiredParameters) {
		filledParams = context.parameters.requiredParameters;
	} else {
		console.log('%%%% Warning: in copyFilledParameters: parent context does not exist; fulfillment = ' + JSON.stringify(fulfillment));
	}
	
	console.log('*** In copyFilledParameters; after copied paremeters from the parent_intent: ' + JSON.stringify(filledParams));
	
	const c = fulfillment._request.queryResult.parameters;
	
	console.log('*** In copyFilledParameters; current intents parameters: ' + JSON.stringify(fulfillment._request.queryResult.parameters));
	
	for (const [name, value] of Object.entries(c)) {
		filledParams[name]=value;
	}
	
	//context.parameters.requiredParameters = applyPostRecoFilter(filledParams, fulfillment,intentName);
	console.log('*** In copyFilledParameters; combined filled parameters in parent intent:  ' + JSON.stringify(context));
	
	if (!isSimpleIntent(intentName)) {
		setParentContext(fulfillment, context.parameters);
	} else {
		setIntentContext(fulfillment, intentName, context.parameters);
	}
	
}

function setIntentContext(fulfillment, intentName, parameters, span) {
	if (span != 0 && !span) {
		span = 5;
	}
	fulfillment.setOutputContext(intentToContextName(intentName), span, parameters);
}

function setParentContext(fulfillment, params) {
	fulfillment.setOutputContext('parent_intent', 20, params);
}

function applyPostRecoFilter(filledParams, fulfillment, intentName) {
	if (isEventCaught(fulfillment)) {
		console.log('*** In applyPostRecoFilter; event caught -- returning filledParams unmodified');
		return filledParams;
	}
	const func = getPostRecoFilter(fulfillment, null, intentName)
	if (func && (func instanceof Function)) {
		return func(fulfillment, filledParams);
	}
	return filledParams;
}

function saveUnconfirmedParameters(fulfillment, intentName) {
	// this function handles both child intents (within top-level intent)
	// and standalone intents (which do not belong to any top level intent);
	// for a standalone intent, intentName will be non-null
		
	var filledParams = {};
		
	
	const c = fulfillment._request.queryResult.parameters;
	
	console.log('*** In saveUnconfirmedParameters; current intents parameters: ' + JSON.stringify(fulfillment._request.queryResult.parameters));
	
	for (const [name, value] of Object.entries(c)) {
		filledParams[name]=value;
	}
	
	var context = {};
	if (isSimpleIntent(intentName)) {
		// handle standalone intent
		context = getIntentContext(fulfillment, intentName);
	} else {
		context = getParentContext(fulfillment);
	}
	if (context && context.parameters) {
		context.parameters.unconfirmed = applyPostRecoFilter(filledParams, fulfillment, intentName);
		console.log('*** In saveUnconfirmedParameters; done copying current intent parameter; parent context is:  ' + JSON.stringify(context));
		if (isSimpleIntent(intentName)) {
		   // handle standalone intent
		   setIntentContext(fulfillment, intentName, context.parameters);
	    } else {
		   setParentContext(fulfillment, context.parameters);
		}
	} else {
		throw Error('saveUnconfirmedParameters: no parent context');
	}
}




function getAllResponseOutputContexts(fulfillment) {
     return fulfillment._response.outputContexts;
}


function intentToEventName(intentName) {
    return intentName.toUpperCase().replace(/ /g, '_');
}

function intentToContextName(intentName) {
    return intentName.toLowerCase().replace(/ /g, '_') + '-context';
}


function endConversation(fulfillment) {
	const lang = getLanguage(fulfillment);
	setEvent(fulfillment, "END_CONVERSATION", lang);
}


function clearAllContexts(fulfillment) {
	const parentContext = getParentContext(fulfillment);
	if (parentContext) {
		parentContext.parameters = {};
		parentContext.parameters.requiredParameters = {};
		setParentContext(fulfillment, parentContext.parameters);
	}
	fulfillment.clearContext('last_intent');
	fulfillment.clearContext('retries_count');
}

// trigger intent to recognize in one of the following modes:
// 'initial' (default) -- initial prompt will play
// 'retry' -- Nomatch/Noinput prompt will play
// 'disconfirm' -- use this when caller said 'no' in a confirmation
function triggerIntent(fulfillment, intentName, params) {
	console.log('*** Entering triggerIntent: intentName = ' + intentName + '; params = ' + JSON.stringify(params) + '; fulfillment = ' + JSON.stringify(fulfillment));
	var mode = 'initial';
	if (intentName == 'RETURN_TO_NLU') {
		clearAllContexts(fulfillment);
		intentName = 'NLU';
		mode = "reentry";
	}
	if (params) {
		if (!params.mode) {
			params.mode = 'initial';
		}
	} else {
		params = {mode:'initial'};
	}
	const lang = getLanguage(fulfillment);
	setEvent(fulfillment, intentToEventName(intentName), lang, params);
	if (intentName != 'NLU') {
		enableIntent(fulfillment, intentName);
	}
	enableGlobalIntents(fulfillment, intentName);
}

// if event triggered the intent, the event parameters will be in the context named as the event
function getEventParameter(fulfillment, paramName) {
	if (isEventCaught(fulfillment)) {
		const eventName = getEventName(fulfillment);
		if (eventName) {
			const c = fulfillment.getContext(eventName.toLowerCase());
			if (c) {
				return c.parameters[paramName];
			}
			return null;
		}
	}
	return null;
}

function getOnIntentTriggered(fulfillment, containerIntentName, intentName) {
	console.log('*** Entering getOnIntentTriggered: fulfillment = ' + JSON.stringify(fulfillment) + '; intentName = ' + intentName);

	var intentConfig = {};
	if (!isSimpleIntent(intentName)) {
		intentConfig = getContainerIntentConfig(containerIntentName);
	} else {
		intentConfig = getSimpleIntentConfig(intentName);
	}
	if (!intentConfig) {
		throw Error(`getOnIntentTriggered(): config for intent ${intentName} and container intent ${containerIntentName} does not exist`);
	}
	
	return intentConfig.onIntentTriggered;
	
}


function clearUnconfirmedParameters(fulfillment, intentName) {
	
	var context = {};
	
	if (isSimpleIntent(intentName)) {
		context = getIntentContext(fulfillment, intentName);
	} else {
		context = getParentContext(fulfillment);
	}
	if (context && context.parameters) {
		context.parameters.unconfirmed = null;
		console.log('**** cleared unconmfirmed params; fulfillment: ' + JSON.stringify(fulfillment));
		
	} else {
		console.log(`%%% WARNING: clearUnconfirmedParameter: No parent context exists`);
	}
	if (isSimpleIntent(intentName)) {
		setIntentContext(fulfillment, intentName, context.parameters);
	} else {
		setParentContext(fulfillment, context.parameters);
	}
}

function getUnconfirmedParameters(fulfillment, intentName) {
	console.log('**** Entering getUnconfirmedParameters; intentName = ' + intentName + '; fulfillment: ' + JSON.stringify(fulfillment));
	var context = {};
	
	
	if (isSimpleIntent(intentName)) {
		context = getIntentContext(fulfillment, intentName);
	} else {
		context = getParentContext(fulfillment);
	}
	if (context && context.parameters) {
		return context.parameters.unconfirmed;
	} else {
		console.log(`%%% WARNING: getUnconfirmedParameters: No parent context exists`);
	}
}

function getUnconfirmedParameterValue(fulfillment, name, intentName) {
	console.log('**** Entering getUnconfirmedParameterValue; name = ' + name + '; fulfillment: ' + JSON.stringify(fulfillment));
	if (!name) {
		throw Error ('getUnconfirmedParameterValue: no parameter name provided');
	}
	const c = getUnconfirmedParameters(fulfillment, intentName);
	console.log('**** in getUnconfirmedParameterValue; unconfirmed params: ' + JSON.stringify(c));
	if (c) {
		return c[name];
	} else {
		return null;
	}
}



function handleIntent(fulfillment, intentName) {
	 console.log('*** Entering handleIntent: intentName = ' + intentName + '; fulfillment = ' + JSON.stringify(fulfillment));
	 try {
		var mode = 'reco'; //TODO: replace with a CONSTANT
		if (isEventCaught(fulfillment)) {
			console.log('*** In handleIntent: event caught');
			mode = getEventParameter(fulfillment, 'mode') || 'initial';
			if (mode == 'initial') {
				console.log('*** In handleIntent: entering intent ' + intentName + '; about to clear the counters');
				clearErrorCounters(fulfillment, null, intentName);
			}
		} 
		if (mode == 'reco') {
			console.log('*** In handleIntent: mode is reco');
			
			const parentIntentName = getParentIntentName(fulfillment);
			if (!isRequiredParameterFilled(fulfillment, parentIntentName, intentName)) { 
				// handle the rare case when the intent was triggered, but the associated parameter is not filled
				// this is essentially a 'nomatch'
				console.log('*** In handleIntent: required parameter for intent ' + intentName + ' is not filled');
				default_fallback_intent(fulfillment, intentName);
			} else {
				
				saveUnconfirmedParameters(fulfillment);
				//console.log('*** In handleIntent: copied filled params: all output contexts = ' + JSON.stringify(fulfillment.getAllOutputContexts()));
				console.log(`*** In handleIntent: required parameter for intent ${intentName} is filled`);
				if (getConfirm(fulfillment, parentIntentName, intentName)) {
					console.log(`*** In handleIntent: required parameter for intent ${intentName} is filled; need to confirm -- triggering confirmation`);
					
					triggerConfirmation(fulfillment, intentName, 'initial');
				} else {
					console.log(`*** In handleIntent: required parameter for intent ${intentName} is filled; do not need to confirm -- returning to parent ${parentIntentName}`);
					copyFilledParameters(fulfillment);
					clearUnconfirmedParameters(fulfillment);
					returnToParentAfterParamFilled(fulfillment, intentName);
				}
			}
		} else {
			//intent has been triggered by event, to kick off filling next parameter
			
			//first, perform any custom pre-recognition logic
			
			const callback = getOnIntentTriggered(fulfillment, getParentIntentName(fulfillment), intentName);
			if (callback) {
				console.log(`*** In handleIntent: onIntentTriggered callback for ${intentName} is ${callback}; about to execute callback function`);
				callback(fulfillment);
			}
			// mode is "initial", "retry", or "disconfirm"
			const promptText = getPrompt(fulfillment, intentName, mode);
			console.log(`*** In handleIntent: mode = ${mode}; promptText = "${promptText}"; intentName = ${intentName}`);
			//addResponseText(fulfillment, promptText); 
			fulfillment.setResponseText(expandParametersInPromptText(fulfillment, promptText));
			enableIntent(fulfillment, intentName);
			
				
			enableGlobalIntents(fulfillment, intentName);
			enableOtherActiveIntents(fulfillment, intentName);
			
		}
	 }catch (e) {
        console.log(e);
        console.log(`%%%%ERROR: caught in handleIntent: ${e.message}`);
    }
}

function saveCurrentIntentName(fulfillment, intentName) {
	if (intentName && intentName.includes(' - ')) {
		// this is a follow up intent name, for example 'BaseMenu - option1'
		// the parent intent name will be before ' - '
		const parentIntentName = intentName.split(' - ')[0];
		intentName = parentIntentName;
	}
	const currentIntentName = getCurrentIntentName(fulfillment);
	if (intentName != currentIntentName) {
		setGlobalParameter(fulfillment, 'intentType', intentName);
		if (isSimpleIntent(currentIntentName)) {
			fulfillment.clearContext(intentToContextName(currentIntentName));
		}
	}
}

function getCurrentIntentName(fulfillment) {
	getGlobalParameter(fulfillment, 'intentType');
}


function handleSimpleIntent(fulfillment, intentName) {
	 console.log('*** Entering handleStandaloneIntent: intentName = ' + intentName + '; fulfillment = ' + JSON.stringify(fulfillment));
	 try {
		if (!isGlobalIntent(intentName)) {
			saveCurrentIntentName(fulfillment, intentName);
		}
		
		
		var mode = 'reco'; //TODO: replace with a CONSTANT
		if (isEventCaught(fulfillment)) {
			console.log('*** In handleIntent: event caught');
			mode = getEventParameter(fulfillment, 'mode') || 'initial';
		} 
		if (mode == 'reco') {
			console.log('*** In handleStandaloneIntent: mode is reco');
			
			if (!isRequiredParameterFilled(fulfillment, null, intentName)) { 
				// handle the rare case when the intent was triggered, but the associated parameter is not filled
				// this is essentially a 'nomatch'
				console.log('*** In handleIntent: required parameter for intent ' + intentName + ' is not filled');
				default_fallback_intent(fulfillment, intentName); //TODO 
			} else {
				
				saveUnconfirmedParameters(fulfillment, intentName);
				console.log(`*** In handleStandaloneIntent: required parameter for intent ${intentName} is filled`);
				if (getConfirm(fulfillment, null, intentName)) { 
					console.log(`*** In handleStandaloneIntent: required parameter for intent ${intentName} is filled; need to confirm -- triggering confirmation`);
					
					triggerConfirmation(fulfillment, intentName, 'initial');
				} else {
					console.log(`*** In handleStandaloneIntent: required parameter for intent ${intentName} is filled; do not need to confirm -- getting next intent`);
					copyFilledParameters(fulfillment, intentName);
					clearUnconfirmedParameters(fulfillment, intentName);
					goToNextForSimpleIntent(fulfillment, intentName); 
				}
			}
		} else {
			//intent has been triggered by event, to kick off filling next parameter
			
			//first, perform any custom pre-recognition logic
			
			const callback = getOnIntentTriggered(fulfillment, null, intentName); 
			if (callback) {
				console.log(`*** In handleIntent: onIntentTriggered callback for ${intentName} is ${callback}; about to execute callback function`);
				callback(fulfillment);
			}
			// mode is "initial", "retry", or "disconfirm"
			const promptText = getPrompt(fulfillment, intentName, mode); 
			console.log(`*** In handleIntent: mode = ${mode}; promptText = "${promptText}"; intentName = ${intentName}`);
			//addResponseText(fulfillment, promptText); 
			fulfillment.setResponseText(expandParametersInPromptText(fulfillment, promptText, intentName)); 
			enableIntent(fulfillment, intentName);		
			enableGlobalIntents(fulfillment, intentName); 
			enableOtherActiveIntents(fulfillment, intentName);
			
		}
	 }catch (e) {
        console.log(e);
        console.log(`%%%%ERROR: caught in handleIntent: ${e.message}`);
    }
}

function isRequiredParameterFilled(fulfillment, parentIntentName, intentName) {
	const paramName = getRequiredParameterName(fulfillment, parentIntentName, intentName);
	console.log('*** In isRequiredParameterFilled: parentIntentName = ' + parentIntentName + '; intentName =' + intentName + '; paramName = ' + paramName + '; fulfillment = ' + JSON.stringify(fulfillment));
	if (!paramName) {
		console.log(`***WARNING: no required param found for ${intentName} intent`);
		return true;
	}
	const value = getParameter(fulfillment, paramName);
	console.log('*** In isRequiredParameterFilled: param value = ' + value);
	return (value != null && value != '' && value != undefined);
	
}

function triggerConfirmation(fulfillment, intentName, mode) {
	const lang = getLanguage(fulfillment);
	console.log('*** Entering triggerConfirmation: intentName = ' + intentName + '; mode = ' + mode);
	setEvent(fulfillment, 'CONFIRM', lang, {intent:intentName, mode:mode});
	enableIntent(fulfillment, 'Confirm');
	
	enableGlobalIntents(fulfillment, intentName);
	enableOtherActiveIntents(fulfillment, intentName);
	
}


function returnToParentAfterParamFilled(fulfillment, intentName) {
	const parentIntentName = getParentIntentName(fulfillment);
	console.log('*** in returnToParentIntent after filling ' + intentName + '; about to return control to parentIntentName: ' + parentIntentName);
	handleContainerIntent(fulfillment, parentIntentName, intentName);
	
}

function makeUnconfrimedParametersFinal(fulfillment, intentName) {	
	
    var context = {};
	
	if (isSimpleIntent(intentName)) {
		context = getIntentContext(fulfillment, intentName);
	} else {
		context = getParentContext(fulfillment);
	}
	if (context && context.parameters && context.parameters.unconfirmed) {
		const c = context.parameters.unconfirmed;
		if (!context.parameters.requiredParameters) {
			context.parameters.requiredParameters = {};
		}
		for (const [name, value] of Object.entries(c)) {
			context.parameters.requiredParameters[name]=value;
		}
		console.log('*** In makeUnconfrimedParametersFinal; copied unconfirmed parameters; combined context: ' + JSON.stringify(context));
		clearUnconfirmedParameters(fulfillment, intentName);
		if (!isSimpleIntent(intentName)) {
			setParentContext(fulfillment, context.parameters);
		} else {
			setIntentContext(fulfillment, intentName, context.parameters);
		}
	} else {
	
		throw Error('makeUnconfrimedParametersFinal: no parent context');
	}
	
}

function handleConfirm(fulfillment) {
	console.log('*** Entering handleConfirm; fulfillment = ' + JSON.stringify(fulfillment));
	try {
		var intentName = getEventParameter(fulfillment, 'intent'); 
		console.log('*** in handleConfirm: intentName = ' + intentName);	
		
		var intentToPass = intentName;
		var parentIntentName = null;
		if (intentName && !isSimpleIntent(intentName)) {
			intentToPass = null;
			parentIntentName = getParentIntentName(fulfillment);
		}
		
		const mode = getEventParameter(fulfillment, 'mode') || 'reco';
		console.log('*** in handleConfirm: mode = ' + mode);
		var promptText = '';
		if (mode == 'initial') {
			promptText = getPrompt(fulfillment, intentName, 'confirm');
			console.log('*** in handleConfirm: mode is "initial"; promptText = ' + promptText);
			fulfillment.setResponseText(expandParametersInPromptText(fulfillment, promptText, intentToPass));
			enableConfirmIntent(fulfillment, intentName );		
		} else if (mode == 'retry' || mode == 'noinput' || mode == 'nomatch' ) {
			promptText = getPrompt(fulfillment, intentName, 'confirm');
			console.log('*** in handleConfirm: mode is "retry"; promptText = ' + promptText);
			fulfillment.setResponseText(expandParametersInPromptText(fulfillment, promptText, intentToPass));
			enableConfirmIntent(fulfillment, intentName);
		} else {
			intentName = getIntentToConfirm(fulfillment);
			intentToPass = intentName;
		    parentIntentName = null;
			if (intentName && !isSimpleIntent(intentName)) {
				intentToPass = null;
				parentIntentName = getParentIntentName(fulfillment);
			}
			const response = getParameter(fulfillment, 'response'); 
			console.log('*** in handleConfirm: performed reco; response is : ' + response + '; intentName = ' + intentName);
			if (response == 'yes') {
				
				console.log('*** in handleConfirm: recognized "yes";');
				makeUnconfrimedParametersFinal(fulfillment, intentToPass);
				if (isSimpleIntent(intentName)) {
					console.log('*** In simple intent: about to trigger the next intent');
					goToNextForSimpleIntent(fulfillment, intentName);
					return;
				}
				console.log('*** In container intent: about to go back to parent ' + parentIntentName);
				returnToParentAfterParamFilled(fulfillment, intentName); 
														
			} else if (response == 'no') {
				clearUnconfirmedParameters(fulfillment, intentToPass);
				const maxDisconfirms = getMaxDisconfirms(fulfillment, parentIntentName, intentName); 
				var numDisconfirms = getNumDisconfirms(fulfillment, intentName); 
				console.log('*** in handleConfirm: recognized "no"; numDisconfirms = ' + numDisconfirms);
				if ((numDisconfirms + 1) < maxDisconfirms) {
					console.log('*** in handleConfirm: recognized "no"; numDisconfirms is less than maxDisconfirms; incrementing disconfirm count');
					incrementDisconfirmCount(fulfillment, intentName);
					console.log('*** in handleConfirm: recognized "no"; about to trigger intent in "disconfirm" mode');
					triggerIntent(fulfillment, intentName, {mode:'disconfirm'});
				} else {
					console.log('*** in handleConfirm: recognized "no"; numDisconfirms is greater than maxDisconfirms; about to throw MAX_DISCONFIRMS event');
					const nextIntent = getMaxDisconfirmsNextIntent(fulfillment, parentIntentName, intentName);
					console.log('*** In handleConfirm; max_disconfirms_next = ' + nextIntent);
					if (nextIntent) {
						triggerIntent(fulfillment, nextIntent);
					} else {
						triggerIntent(fulfillment, getDefaultMaxDisconfirmsNextIntent(), {intentType:intentName});
					}
																							
				}
			}
		}
	} catch (e) {
		console.log('%%%%ERROR: Caught error in handleConfirm():' + e.message);
	}
	
}

function getBargeIn() { 
	var bargein = getDefaults().bargein;
	if (bargein != undefined && bargein != null) {
		return bargein;
	}
	return true;
}

function getDefaultGlobals() { 
	return getDefaults().globals ? getDefaults().globals : [];
}

function getDefaultMaxDisconfirmsNextIntent() { // TODO: make dynamic
	return getDefaults().max_disconfirms_next;
}

function getDefaultMaxRetriesNextIntent() { // TODO: make dynamic
	return getDefaults().max_retries_next;
}

function getDefaultMaxNoinputsNextIntent() { // TODO: make dynamic
	if (getDefaults().max_noinputs_next) {
		return getDefaults().max_noinputs_next;
	}
	return getDefaultMaxRetriesNextIntent();
}

function getDefaultMaxNomatchesNextIntent() { // TODO: make dynamic
	if (getDefaults().max_nomatches_next) {
		return getDefaults().max_nomatches_next;
	}
	return getDefaultMaxRetriesNextIntent();
}

function getDefaultMaxDisconfirms() { // TODO: make dynamic
	return getDefaults().max_disconfirms;
}

function localize(fulfillment, prompt) {
	console.log('*** Entering localize; prompt = ' + JSON.stringify(prompt));
	if (!prompt) {
		return null;
	}
	
	if ((prompt instanceof String) || (prompt instanceof Function) || (typeof prompt === 'string')) { 
		console.log('*** In localize: prompt is a String or a Function -- returning: ' + (prompt instanceof Function) ? '(Function)' : prompt);
		return prompt;
	}
	
	if (prompt instanceof Object) {
		console.log('*** prompt is an Array ');
		const lang = getLanguage(fulfillment);
		console.log('*** language is: ' + lang);
		return prompt[lang];
	}
	return prompt;
}

// mode has to be one of: 'initial', 'retry', 'reentry', 'disconfirm', 'confirm'

function getPrompt(fulfillment, intentName, mode) {
	console.log('*** Entering getPrompt; intentName = ' + intentName + '; mode = ' + mode);
	try {
		var prompt = '';
		
		if (intentName == 'NLU') {
			
			const intentConfig = getContainerIntentConfig('NLU');
			console.log('**** In getPrompt for NLU: intentConfig is: ' + JSON.stringify(intentConfig));
			if (!intentConfig) {
				console.log (`**** WARNING: no config for NLU`);
				return getDefaultFulfillmentText(fulfillment);
			}
			if (intentConfig.prompts) {
				if ((mode == 'noinput' || mode == 'nomatch') && !intentConfig.prompts[mode] && !intentConfig.prompts[mode + "1"] && !intentConfig.prompts[mode + "2"] && !intentConfig.prompts[mode + "3"]) {
					mode = 'retry';
				}
				console.log('**** In getPrompt for NLU: new mode is: ' + mode + '; about to localize and resolve prompt: ' + JSON.stringify(intentConfig.prompts[mode]));
				return resolve(fulfillment, localize(fulfillment, getHighestCountPrompt(fulfillment, intentConfig.prompts,intentName, mode)));
			}
			console.log('**** WARNING: NLU has no prompts in config object');
			return '';
		}
		if (isContainerIntent(intentName)) {
			// this is really a welcome intent
			const intentConfig = getContainerIntentConfig(intentName);
			if (!intentConfig) {
				console.log (`**** WARNING: no config for intent ${intentName}`);
				return getDefaultFulfillmentText(fulfillment);
			}
			return resolve(fulfillment, localize(fulfillment,intentConfig.prompt));
		}
		
		if (isSimpleIntent(intentName)) { 
			const intentConfig = getSimpleIntentConfig(intentName);
			if (!intentConfig || !intentConfig.prompts) {
				console.log (`**** WARNING: no config for standalone intent ${intentName}`);
				return '';
			}
			if ((mode == 'noinput' || mode == 'nomatch') && !intentConfig.prompts[mode] && !intentConfig.prompts[mode + "1"] && !intentConfig.prompts[mode + "2"] && !intentConfig.prompts[mode + "3"]) { // TODO: encapsulate in a function
					mode = 'retry';
			}
			return resolve(fulfillment, localize(fulfillment,getHighestCountPrompt(fulfillment, intentConfig.prompts,intentName, mode)));
		}
		const parentIntentName = getParentIntentName(fulfillment);
		if (!parentIntentName || parentIntentName == '') {
			console.log (`**** WARNING: no config for parent intent found when in ${intentName}`);
			return getDefaultFulfillmentText(fulfillment);
		}
		const intentConfig = getContainerIntentConfig(parentIntentName);
		if (!intentConfig) {
			console.log (`**** WARNING: no config for intent ${parentIntentName}`);
			return getDefaultFulfillmentText(fulfillment);
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				if ((mode == 'noinput' || mode == 'nomatch') && !paramConfig.prompts[mode] && !paramConfig.prompts[mode + "1"] && !paramConfig.prompts[mode + "2"] && !paramConfig.prompts[mode + "3"]) { //TODO: encapsulate in a function
					mode = 'retry';
				}
				var promptText = getHighestCountPrompt(fulfillment, paramConfig.prompts,intentName, mode);
				if (promptText) {
					return resolve(fulfillment, localize(fulfillment,promptText));
				}
				if (!promptText && mode == 'reentry') {
					//if reentry prompt does not exist, use initial
					mode = 'initial';
					return resolve(fulfillment, localize(fulfillment,paramConfig.prompts[mode])) || '';
				}
			}
		}
		return '';
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getPrompt(): ' + e);
	}
}

function getHighestCount(fulfillment, intentName, mode) {
	if (mode == 'noinput') {
		return getNumNoinputs(fulfillment, intentName);
	}
	
	if (mode == 'nomatch') {
		return getNumNomatches(fulfillment, intentName);
	}
	
	if (mode == 'retry') {
		return getNumRetries(fulfillment, intentName);
	}
	
	if (mode == 'disconfirm') {
		return getNumDisconfirms(fulfillment, intentName);
	}
	return 0;
}

function getHighestCountPrompt(fulfillment, prompts, intentName, mode) {
	console.log('*** In getHighestCountPrompt: intent = ' + intentName + ' mode = ' + mode + ' prompts = ' + JSON.stringify(prompts));
	if (mode != 'retry' && mode != 'noinput' && mode != 'nomatch' && mode != 'disconfirm') {
		return prompts[mode]; // don't do counts for initial and reentry prompts
	}
	
	var count = getHighestCount(fulfillment, intentName, mode) || 0;
	console.log('*** In getHighestCountPrompt: got count = ' + count);
	for (var i = count; i > -1; i--) {
		
		console.log('*** In getHighestCountPrompt: i = ' + i);
		var modeWithCounter = mode;
		if (i > 0) {
			modeWithCounter = modeWithCounter + i;
		}
		console.log('*** In getHighestCountPrompt: modeWithCounter = ' + modeWithCounter);
		if (prompts[modeWithCounter]) {
			console.log('*** In getHighestCountPrompt: returning prompt: ' + prompts[modeWithCounter]);
			return prompts[modeWithCounter];
		}
	}
	
	console.log('*** In getHighestCountPrompt: did not find any prompts; returning null');
	return null;
	
}

function getNextForTopLevelIntent(fulfillment, intentName) {
	try {
			var nextIntent = '';
			if (!isContainerIntent(intentName)) {
				return null;
			}
			const intentConfig = getContainerIntentConfig(intentName);
			if (!intentConfig) {
				console.log (`**** WARNING: in getNextTopLevelIntent: no config for top-level intent ${intentName}`);
				return null;
			}
			console.log (`++++ in getNextForTopLevelIntent: calling resolve on "next" for top-level intent ${intentName}`); 
			return resolve(fulfillment, intentConfig.next);
		
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getNextForTopLevelIntent(): ' + e);
	}
}

function getMaxRetriesNextIntent(fulfillment, parentIntentName, intentName) {
	console.log('**** Entering getMaxRetriesNextIntentName for ' + intentName + ' with parent intent: ' + parentIntentName + '; fulfillment: ' + JSON.stringify(fulfillment));
	try {
		if (intentName == 'NLU') {
			const intentConfig = getContainerIntentConfig('NLU');
			if (!intentConfig) {
				console.log (`**** WARNING: getMaxRetriesNextIntent: no config for NLU`);
				clearErrorCounters(fulfillment, parentIntentName, intentName);
				return getDefaultMaxRetriesNextIntent();
			}
			if (intentConfig.max_retries_next) {
				clearErrorCounters(fulfillment, parentIntent, intentName);
				return resolve(fulfillment, intentConfig.max_retries_next);
			}
			console.log('****  NLU has no max_retries_next specified -- returning default');
			clearErrorCounters(fulfillment, parentIntentName, intentName);
			return getDefaultMaxRetriesNextIntent();
		}
		var intentConfig = {};
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName); 
			if (!intentConfig) {
				console.log(`++++ WARNING: getMaxRetriesNextIntent: no config for simple intent ${intentName}`);
				return false;
			}
			const max_retries_next = resolve(fulfillment, intentConfig.max_retries_next);
			console.log(`++++ getMaxRetriesNextIntent:  "max_retries_next" for standalone intent ${intentName} resolved to: ${max_retries_next}`);
			clearErrorCounters(fulfillment, parentIntentName, intentName);
			return max_retries_next;
		} 
		intentConfig = getContainerIntentConfig(parentIntentName);	
		console.log('**** In getMaxRetriesNextIntentName; intentConfig for ' + parentIntentName + ' is: ' + JSON.stringify(intentConfig));
		if (!intentConfig) {
			console.log (`**** WARNING: in getMaxRetriesNextIntentName: no config for intent ${parentIntentName}`);
			return null;
		}
		
		if (!intentConfig.requiredParameters) {
			console.log (`**** WARNING: in getMaxRetriesNextIntentName: no required params for intent ${parentIntentName}`);
			return null;
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				console.log (`++++ in getMaxRetriesNextIntentName: calling resolve on "next" for top-level intent ${parentIntentName} and child intent ${intentName}`);
				clearErrorCounters(fulfillment, parentIntentName, intentName);
				return resolve(fulfillment, paramConfig.max_retries_next);
			}
		}
		return null;
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getMaxRetriesNextIntentName(): ' + e.message);
		return null;

	}
	
}

function getMaxNoinputsNextIntent(fulfillment, parentIntentName, intentName) {
	console.log('**** Entering getMaxNoinputsNextIntentName for ' + intentName + ' with parent intent: ' + parentIntentName + '; fulfillment: ' + JSON.stringify(fulfillment));
	try {
		if (intentName == 'NLU') {
			const intentConfig = getContainerIntentConfig('NLU');
			if (!intentConfig) {
				console.log (`**** WARNING: getMaxNoinputsNextIntent: no config for NLU`);
				return getDefaultMaxNoinputsNextIntent();
			}
			if (intentConfig.max_noinputs_next) {
				return resolve(fulfillment, intentConfig.max_noinputs_next);
			}
			console.log('****  NLU has no max_noinputs_next specified -- returning default');
			return getDefaultMaxNoinputsNextIntent();
		}
		var intentConfig = {};
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName); 
			if (!intentConfig) {
				console.log(`++++ WARNING: getMaxNoinputsNextIntent: no config for simple intent ${intentName}`);
				return false;
			}
			const max_noinputs_next = resolve(fulfillment, intentConfig.max_noinputs_next);
			console.log(`++++ getMaxNoinputsNextIntent:  "max_Noinputs_next" for standalone intent ${intentName} resolved to: ${max_noinputs_next}`);
			return max_noinputs_next;
		} 
		intentConfig = getContainerIntentConfig(parentIntentName);	
		console.log('**** In getMaxNoinputsNextIntentName; intentConfig for ' + parentIntentName + ' is: ' + JSON.stringify(intentConfig));
		if (!intentConfig) {
			console.log (`**** WARNING: in getMaxNoinputsNextIntentName: no config for intent ${parentIntentName}`);
			return null;
		}
		
		if (!intentConfig.requiredParameters) {
			console.log (`**** WARNING: in getMaxNoinputsNextIntentName: no required params for intent ${parentIntentName}`);
			return null;
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				console.log (`++++ in getMaxNoinputsNextIntentName: calling resolve on "next" for top-level intent ${parentIntentName} and child intent ${intentName}`);
				return resolve(fulfillment, paramConfig.max_noinputs_next);
			}
		}
		return null;
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getMaxNoinputsNextIntentName(): ' + e.message);
		return null;

	}
	
}

function getMaxNomatchesNextIntent(fulfillment, parentIntentName, intentName) {
	console.log('**** Entering getMaxNomatchesNextIntentName for ' + intentName + ' with parent intent: ' + parentIntentName + '; fulfillment: ' + JSON.stringify(fulfillment));
	try {
		if (intentName == 'NLU') {
			const intentConfig = getContainerIntentConfig('NLU');
			if (!intentConfig) {
				console.log (`**** WARNING: getMaxNomatchesNextIntent: no config for NLU`);
				return getDefaultMaxNomatchesNextIntent();
			}
			if (intentConfig.max_nomatches_next) {
				return resolve(fulfillment, intentConfig.max_nomatches_next);
			}
			console.log('****  NLU has no max_nomatches_next specified -- returning default');
			return getDefaultMaxNomatchesNextIntent();
		}
		var intentConfig = {};
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName); 
			if (!intentConfig) {
				console.log(`++++ WARNING: getMaxNomatchesNextIntent: no config for simple intent ${intentName}`);
				return false;
			}
			const max_noinputs_next = resolve(fulfillment, intentConfig.max_noinputs_next);
			console.log(`++++ getMaxNomatchesNextIntent:  "max_nomatches_next" for standalone intent ${intentName} resolved to: ${max_nomatches_next}`);
			return max_nomatches_next;
		} 
		intentConfig = getContainerIntentConfig(parentIntentName);	
		console.log('**** In getMaxNomatchesNextIntentName; intentConfig for ' + parentIntentName + ' is: ' + JSON.stringify(intentConfig));
		if (!intentConfig) {
			console.log (`**** WARNING: in getMaxNomatchesNextIntentName: no config for intent ${parentIntentName}`);
			return null;
		}
		
		if (!intentConfig.requiredParameters) {
			console.log (`**** WARNING: in getMaxNomatchesNextIntentName: no required params for intent ${parentIntentName}`);
			return null;
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				console.log (`++++ in getMaxNomatchesNextIntentName: calling resolve on "next" for top-level intent ${parentIntentName} and child intent ${intentName}`);
				return resolve(fulfillment, paramConfig.max_nomatches_next);
			}
		}
		return null;
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getMaxNomatchesNextIntentName(): ' + e.message);
		return null;

	}
	
}

function getMaxDisconfirmsNextIntent(fulfillment, parentIntentName, intentName) {
	console.log('**** Entering getMaxDisconfirmsNextIntentName for ' + intentName + ' with parent intent: ' + parentIntentName + '; fulfillment: ' + JSON.stringify(fulfillment));
	try {
		
		if (intentName == 'NLU') {
			const intentConfig = getContainerIntentConfig('NLU');
			if (!intentConfig) {
				console.log (`**** WARNING: getMaxDisconfirmsNextIntent: no config for NLU`);
				var next = getDefaultMaxDisconfirmsNextIntent();
				clearErrorCounters(fulfillment, parentIntentName, intentName);
				return next;
			}
			if (intentConfig.max_disconfirms_next) {
				var next = resolve(fulfillment, intentConfig.max_disconfirms_next);
				clearErrorCounters(fulfillment, parentIntentName, intentName);
				return next;
			}
			console.log('****  NLU has no max_retried specified -- returning default');
			var next = getDefaultMaxDisconfirmsNextIntent();
			clearErrorCounters(fulfillment, parentIntentName, intentName);
			return next;
		}
		var intentConfig = {};
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName); 
			if (!intentConfig) {
				console.log(`++++ WARNING: getMaxDisconfirmsNextIntent: no config for simple intent ${intentName}`);
				return false;
			}
			const max_disconfirms_next = resolve(fulfillment, intentConfig.max_disconfirms_next);
			console.log(`++++ getMaxDisconfirmsNextIntent:  "max_disconfirms_next" for standalone intent ${intentName} resolved to: ${max_disconfirms_next}`);
			clearErrorCounters(fulfillment, parentIntentName, intentName);
			return max_disconfirms_next;
		} 
		   
		intentConfig = getContainerIntentConfig(parentIntentName);	
		
		console.log('**** In getMaxDisconfirmsNextIntentName; intentConfig for ' + parentIntentName + ' is: ' + JSON.stringify(intentConfig));
		if (!intentConfig) {
			console.log (`**** WARNING: in getMaxDisconfirmsNextIntentName: no config for intent ${parentIntentName}`);
			return null;
		}
		
		if (!intentConfig.requiredParameters) {
			console.log (`**** WARNING: in getMaxDisconfirmsNextIntentName: no required params for intent ${parentIntentName}`);
			return null;
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				console.log (`++++ in getMaxDisconfirmsNextIntentName: calling resolve on "next" for top-level intent ${parentIntentName} and child intent ${intentName}`);
				
				var next = resolve(fulfillment, paramConfig.max_disconfirms_next);
				clearErrorCounters(fulfillment, parentIntentName, intentName);
				return next;
			}
		}
		return null;
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getMaxDisconfirmsNextIntentName(): ' + e.message);
		return null;

	}
	
}


function clearErrorCounters(fulfillment, parentIntentName, intentName) {
	console.log('**** Entering clearErrorCounters for ' + intentName + ' with parent intent: ' + parentIntentName + '; fulfillment: ' + JSON.stringify(fulfillment));
	var context = {};
	
	if (isSimpleIntent(intentName)) {
		context = getIntentContext(fulfillment, intentName);
	} else {
		context = getParentContext(fulfillment);
	}
	if (context && context.parameters) {
		context.parameters[intentName + '-disconfirms'] = 0;
		context.parameters[intentName + '-retries'] = 0;
		context.parameters[intentName + '-noinputs'] = 0;
		context.parameters[intentName + '-nomatches'] = 0;
		
		context.parameters['Confirm-retries'] = 0;
		console.log('***clearErrorCounters: after setting errors to 0: context is: ' + JSON.stringify(context)); 
		setGlobalParameter(fulfillment, intentName, context.parameters.requiredParameters);
		if (!isSimpleIntent(intentName)) {
			setParentContext(fulfillment, context.parameters);
		} else {
			setIntentContext(fulfillment, intentName, context.parameters);
			console.log('***clearErrorCounters: after setting errors to 0: fulfillment is: ' + JSON.stringify(fulfillment)); 
			fulfillment.clearContext(intentToContextName(intentName));
			console.log('***clearErrorCounters: after clearing the context ' + intentToContextName(intentName) + ': fulfillment is: ' + JSON.stringify(fulfillment)); 
		}
	}
	
}

function getNextIntentName(fulfillment, parentIntentName, intentName) {
	console.log('**** Entering getNextIntentName for ' + intentName + ' with parent intent: ' + parentIntentName + '; fulfillment: ' + JSON.stringify(fulfillment));
	try {
		
		var intentConfig = {};
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName);
			console.log (`++++ in getNextIntentName: calling resolve on "next"  for simple intent ${intentName}`);
			const nextIntent =  resolve(fulfillment, intentConfig.next);
			clearErrorCounters(fulfillment, null, intentName);
			return nextIntent;
		} else {
		  intentConfig = getContainerIntentConfig(parentIntentName);
		}
		
		console.log('**** In getNextIntentName; intentConfig is: ' + JSON.stringify(intentConfig));
		if (!intentConfig) {
			console.log (`**** WARNING: in getNextIntentName: no config for intent ${parentIntentName}`);
			return null;
		}
		
		if (!intentConfig.requiredParameters) {
			console.log (`**** WARNING: in getNextIntentName: no required params for intent ${parentIntentName}`);
			return null;
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				console.log (`++++ in getNextIntentName: calling resolve on "next" for top-level intent ${parentIntentName} and child intent ${intentName}`);
				const nextIntent = resolve(fulfillment, paramConfig.next);
				clearErrorCounters(fulfillment, parentIntentName, intentName);
				return nextIntent;
			}
		}
		return null;
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getNextIntentName(): ' + e.message);
		return null;
	}
}

function getPostRecoFilter(fulfillment, parentIntentName, intentName) {
	console.log('**** Entering getPostRecoFilter for ' + intentName + ' with parent intent: ' + parentIntentName + '; fulfillment: ' + JSON.stringify(fulfillment));
	try {
		
		var intentConfig = {};
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName);
			console.log (`++++ in getPostRecoFilter:  simple intent ${intentName}`);
			const func =  intentConfig.postRecoFilter;
			if (func && (func instanceof Function)) {
				console.log ('++++ in getPostRecoFilter: found function ' + func.name);
			} else {
				console.log ('++++ in getPostRecoFilter: did not find filter ');
			}
			return func;
		} else {
		  intentConfig = getContainerIntentConfig(parentIntentName);
		}
		
		console.log('**** In getPostRecoFilter; intentConfig is: ' + JSON.stringify(intentConfig));
		if (!intentConfig) {
			console.log (`**** WARNING: in getPostRecoFilter: no config for intent ${parentIntentName}`);
			return null;
		}
		
		if (!intentConfig.requiredParameters) {
			console.log (`**** WARNING: in getPostRecoFilter: no required params for intent ${parentIntentName}`);
			return null;
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				console.log (`++++ in getPostRecoFilter: top-level intent ${parentIntentName} and child intent ${intentName}`);
				const func =  paramConfig.postRecoFilter;				
				return func;
			}
		}
		return null;
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getPostRecoFilter(): ' + e.message);
		return null;
	}
}

function getConfirm(fulfillment, parentIntentName, intentName) {
	try {
		var intentConfig = {};
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName); 
			if (!intentConfig) {
				console.log(`++++ WARNING: no config for simple intent ${intentName}`);
				return false;
			}
			const confirm = resolve(fulfillment, intentConfig.confirm);
			console.log(`++++ getConfirm:  "confirm" for standalone intent ${intentName} resolved to: ${confirm}`);
			return confirm;
		} else {
		   intentConfig = getContainerIntentConfig(parentIntentName);
		}
		if (!intentConfig) {
			console.log (`**** WARNING: getConfirm: no config for intent ${parentIntentName}`);
			return false;
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				console.log(`++++ getConfirm: calling resolve on "confirm" for top-level intent ${parentIntentName} and child intent ${intentName}`);
				const confirm = resolve(fulfillment, paramConfig.confirm);
				console.log(`++++ getConfirm:  "confirm" for top-level intent ${parentIntentName} and child intent ${intentName} resolved to: ${confirm}`);
				if (confirm != null) {
					return confirm;
				}
				console.log(`++++ getConfirm:  "confirm" for top-level intent ${parentIntentName} and child intent ${intentName} resolved to null -- returning false`);
				return false;
			}
		}
		console.log(`++++ WARNING: getConfirm:  could not find matching intent for top-level intent ${parentIntentName} and child intent ${intentName}; -- returning false`);
		return false;
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getNextIntentName(): ' + e);
		return false;
	}
}

function getMaxRetries(fulfillment, parentIntentName, intentName) {
	try {
		
		var intentConfig = {};
		if (intentName == 'NLU') {
			const intentConfig = getContainerIntentConfig('NLU');
			if (!intentConfig) {
				console.log (`**** WARNING: getMaxRetries: no config for NLU`);
				return getDefaultMaxRetries();
			}
			if ((intentConfig.max_retries != undefined) && (intentConfig.max_retries != null)) {
				return resolve(fulfillment, intentConfig.max_retries);
			}
			console.log('****  NLU has no max_retried specified -- returning default');
			return getDefaultMaxRetries();
		}
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName); 
			if (!intentConfig || (intentConfig.max_retries == undefined) || (intentConfig.max_retries == null)) {
				console.log(`++++  getMaxRetries: max_retries for simple intent ${intentName} not specified; returning default`);
				return getDefaultMaxRetries();
			}
			const max_retries = resolve(fulfillment, intentConfig.max_retries);
			console.log(`++++ getMaxRetries:  "max_retries" for standalone intent ${intentName} resolved to: ${max_retries}`);
			return max_retries;
		} 
		intentConfig = getContainerIntentConfig(parentIntentName);
		if (!intentConfig) {
			console.log (`**** WARNING: getMaxRetries: no config for intent ${parentIntentName}`);
			return getDefaultMaxRetries();
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				console.log(`++++ getMaxRetries: calling resolve on "max_retries" for top-level intent ${parentIntentName} and child intent ${intentName}`);
				const maxRetries = resolve(fulfillment, paramConfig.max_retries);
				console.log(`++++ getMaxRetries:  "confirm" for top-level intent ${parentIntentName} and child intent ${intentName} resolved to: ${maxRetries}`);
				if (maxRetries != null) {
					return maxRetries;
				}
				console.log(`++++ getMaxRetries:  "maxRetries" for top-level intent ${parentIntentName} and child intent ${intentName} resolved to null -- returning false`);
				return getDefaultMaxRetries();
			}
		}
		console.log(`++++ WARNING: getMaxRetries:  could not find matching intent for top-level intent ${parentIntentName} and child intent ${intentName}; -- returning default`);
		return getDefaultMaxRetries();
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getMaxRetries(): ' + e);
		return getDefaultMaxRetries();
	}
}

function getActiveIntents(fulfillment, intentName) {
	try {
		console.log('**** Entering getActiveIntents; intentName: ' + intentName);
		var intentConfig = {};
		if (intentName == 'NLU') {
			const intentConfig = getContainerIntentConfig('NLU');
			if (!intentConfig) {
				console.log (`**** WARNING: getActiveIntents: no config for NLU`);
				return [];
			}
			if (intentConfig.active_intents) {
				return resolve(fulfillment, intentConfig.active_intents);
			}
			console.log('****  NLU has no intentConfig.active_intents specified -- returning empty set');
			return [];
		}
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName); 
			if (!intentConfig || !intentConfig.active_intents) {
				console.log(`++++  getActiveIntents: active_intents for simple intent ${intentName} not specified; returning default`);
				return [];
			}
			const active_intents = resolve(fulfillment, intentConfig.active_intents);
			console.log(`++++ getActiveIntents:  "active_intents" for standalone intent ${intentName} resolved to: ${active_intents}`);
			return active_intents;
		} 
		
		if (isChildIntent(intentName)) {
			const parentIntentName = getParentIntentName(fulfillment, intentName);
			intentConfig = getContainerIntentConfig(parentIntentName);
			if (!intentConfig) {
				console.log (`**** WARNING: getActiveIntents: no config for intent ${parentIntentName}`);
				return [];
			}
			for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
				const paramConfig = intentConfig.requiredParameters[i];
				if (paramConfig.intentName == intentName) {
					console.log(`++++ getActiveIntents: calling resolve on "active_intents" for top-level intent ${parentIntentName} and child intent ${intentName}`);
					const active_intents = resolve(fulfillment, paramConfig.active_intents);
					console.log(`++++ getActiveIntents:  "active_intents" for top-level intent ${parentIntentName} and child intent ${intentName} resolved to: ${active_intents}`);
					if (active_intents != null) {
						return active_intents;
					}
					console.log(`++++ getActiveIntents:  "active_intents" for top-level intent ${parentIntentName} and child intent ${intentName} resolved to null -- returning []`);
					return [];
				}
			}
			console.log(`++++ WARNING: getActiveIntents:  could not find matching intent for top-level intent ${parentIntentName} and child intent ${intentName}; -- returning []`);
			return [];
		}
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getActiveIntents(): ' + e);
		return [];
	}
}


function getMaxNoinputs(fulfillment, parentIntentName, intentName) {
	try {
		
		var intentConfig = {};
		if (intentName == 'NLU') {
			const intentConfig = getContainerIntentConfig('NLU');
			if (!intentConfig) {
				console.log (`**** WARNING: getMaxNoinputs: no config for NLU`);
				return getDefaultMaxNoinputs();
			}
			if ((intentConfig.max_noinputs != undefined) && (intentConfig.max_noinputs != null)) {
				return resolve(fulfillment, intentConfig.max_noinputs);
			}
			console.log('****  NLU has no max_noinputs specified -- returning default');
			return getDefaultMaxNoinputs();
		}
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName); 
			if (!intentConfig || (intentConfig.max_noinputs == undefined) || (intentConfig.max_noinputs == null)) {
				console.log(`++++  getMaxNoinputs: max_noinputs for simple intent ${intentName} not specified; returning default`);
				return getDefaultMaxNoinputs();
			}
			const max_noinputs = resolve(fulfillment, intentConfig.max_noinputs);
			console.log(`++++ getMaxNoinputs:  "max_noinputs" for standalone intent ${intentName} resolved to: ${max_noinputs}`);
			return max_noinputs;
		} 
		intentConfig = getContainerIntentConfig(parentIntentName);
		if (!intentConfig) {
			console.log (`**** WARNING: getMaxNoinputs: no config for intent ${parentIntentName}`);
			return getDefaultMaxNoinputs();
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				console.log(`++++ getMaxNoinputs: calling resolve on "max_noinputs" for top-level intent ${parentIntentName} and child intent ${intentName}`);
				const maxNoinputs = resolve(fulfillment, paramConfig.max_noinputs);
				console.log(`++++ getMaxNoinputs:  "confirm" for top-level intent ${parentIntentName} and child intent ${intentName} resolved to: ${maxNoinputs}`);
				if (maxNoinputs != null) {
					return maxNoinputs;
				}
				console.log(`++++ getMaxNoinputs:  "maxNoinputs" for top-level intent ${parentIntentName} and child intent ${intentName} resolved to null -- returning false`);
				return getDefaultMaxNoinputs();
			}
		}
		console.log(`++++ WARNING: getMaxNoinputs:  could not find matching intent for top-level intent ${parentIntentName} and child intent ${intentName}; -- returning default`);
		return getDefaultMaxNoinputs();
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getMaxNoinputs(): ' + e);
		return getDefaultMaxNoinputs();
	}
}

function getMaxNomatches(fulfillment, parentIntentName, intentName) {
	try {
		
		var intentConfig = {};
		if (intentName == 'NLU') {
			const intentConfig = getContainerIntentConfig('NLU');
			if (!intentConfig) {
				console.log (`**** WARNING: getMaxNomatches: no config for NLU`);
				return getDefaultMaxNomatches();
			}
			if ((intentConfig.max_nomatches != undefined) && (intentConfig.max_nomatches != null)) {
				return resolve(fulfillment, intentConfig.max_nomatches);
			}
			console.log('****  NLU has no max_nomatches specified -- returning default');
			return getDefaultMaxNomatches();
		}
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName); 
			if (!intentConfig || (intentConfig.max_nomatches == undefined) || (intentConfig.max_nomatches == null)) {
				console.log(`++++  getMaxNomatches: max_nomatches for simple intent ${intentName} not specified; returning default`);
				return getDefaultMaxNomatches();
			}
			const max_nomatches = resolve(fulfillment, intentConfig.max_nomatches);
			console.log(`++++ getMaxNomatches:  "max_nomatches" for standalone intent ${intentName} resolved to: ${max_nomatches}`);
			return max_nomatches;
		} 
		intentConfig = getContainerIntentConfig(parentIntentName);
		if (!intentConfig) {
			console.log (`**** WARNING: getMaxNomatches: no config for intent ${parentIntentName}`);
			return getDefaultMaxNomatches();
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				console.log(`++++ getMaxNomatches: calling resolve on "max_nomatches" for top-level intent ${parentIntentName} and child intent ${intentName}`);
				const maxNomatches = resolve(fulfillment, paramConfig.max_nomatches);
				console.log(`++++ getMaxNomatches:  "confirm" for top-level intent ${parentIntentName} and child intent ${intentName} resolved to: ${maxNomatches}`);
				if (maxNomatches != null) {
					return maxNomatches;
				}
				console.log(`++++ getMaxNomatches:  "maxNomatches" for top-level intent ${parentIntentName} and child intent ${intentName} resolved to null -- returning false`);
				return getDefaultMaxNomatches();
			}
		}
		console.log(`++++ WARNING: getMaxNomatches:  could not find matching intent for top-level intent ${parentIntentName} and child intent ${intentName}; -- returning default`);
		return getDefaultMaxNomatches();
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getMaxNomatches(): ' + e);
		return getDefaultMaxNomatches();
	}
}

function isParameterDefinedForIntent(fulfillment, parentIntentName, intentName, paramName) {
	try {
		
		var intentConfig = {};
		if (intentName == 'NLU') {
			const intentConfig = getContainerIntentConfig('NLU');
			if (!intentConfig) {
				console.log (`**** WARNING: isParameterDefinedForIntent: no config for NLU`);
				return false;
			}
			if (intentConfig[paramName]) {
				return true;
			}
			
			return false;
		}
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName); 
			if (!intentConfig || !intentConfig[paramName]) {
				console.log(`++++  isParameterDefinedForIntent: ${paramName} for simple intent ${intentName} not specified; returning false`);
				return false;
			}
			
			if (intentConfig[paramName]) {
				return true;
			} else {
				return false;
			}
		} 
		intentConfig = getContainerIntentConfig(parentIntentName);
		if (!intentConfig) {
			console.log (`**** WARNING: isParameterDefinedForIntent: no config for intent ${parentIntentName}`);
			return false;
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				
				if (paramConfig[paramName]) {
					return true;
				}
				
				return false;
			}
		}
		
		return false;
	} catch (e) {
		console.log('%%%%ERROR: Error caught in isParameterDefinedForIntent(): ' + e);
		return false;
	}
}

function getMaxDisconfirms(fulfillment, parentIntentName, intentName) {
	try {
		
		if (intentName == 'NLU') {
			const intentConfig = getContainerIntentConfig('NLU');
			if (!intentConfig) {
				console.log (`**** WARNING: getMaxDisconfirms: no config for NLU`);
				return getDefaultMaxDisconfirms();
			}
			if (intentConfig.max_disconfirms) {
				return resolve(fulfillment, intentConfig.max_disconfirms);
			}
			console.log('****  NLU has no max_disconfirms specified -- returning default');
			return getDefaultMaxDisconfirms();
		}
		if (isSimpleIntent(intentName)) {
			console.log (`****  getMaxDisconfirms: in simple intent ${intentName}`);
			let intentConfig = getSimpleIntentConfig(intentName);
			if (!intentConfig || !intentConfig.max_disconfirms || !resolve(fulfillment, intentConfig.max_disconfirms)) {
				console.log(`++++ getMaxDisconfirms:  "maxDisconfirms" for simple intent ${intentName} resolved to null -- returning default`);
				return getDefaultMaxDisconfirms();
			}
			console.log(`++++ getMaxDisconfirms:  "maxDisconfirms" for simple intent ${intentName} is not null -- calling resolve`);
			const maxDisconfirms = resolve(fulfillment, intentConfig.max_disconfirms);
			console.log(`++++ getMaxDisconfirms:  "maxDisconfirms" for simple intent ${intentName} resolved to ${maxDisconfirms}`);
			return maxDisconfirms;			
		}
		const intentConfig = getContainerIntentConfig(parentIntentName);
		if (!intentConfig) {
			console.log (`**** WARNING: getMaxDisconfirms: no config for intent ${parentIntentName}`);
			return getDefaultMaxDisconfirms();
		}
		for (var i = 0; i < intentConfig.requiredParameters.length; i++) {
			const paramConfig = intentConfig.requiredParameters[i];
			if (paramConfig.intentName == intentName) {
				console.log(`++++ getMaxDisconfirms: calling resolve on "max_disconfirms" for top-level intent ${parentIntentName} and child intent ${intentName}`);
				const maxDisconfirms = resolve(fulfillment, paramConfig.max_disconfirms);
				console.log(`++++ getMaxDisconfirms:  "confirm" for top-level intent ${parentIntentName} and child intent ${intentName} resolved to: ${maxDisconfirms}`);
				if (maxDisconfirms != null) {
					return maxDisconfirms;
				}
				console.log(`++++ getDisconfirms:  "maxDisconfirms" for top-level intent ${parentIntentName} and child intent ${intentName} resolved to null -- returning false`);
				return getDefaultMaxDisconfirms();
			}
		}
		console.log(`++++ WARNING: getMaxDisconfirms:  could not find matching intent for top-level intent ${parentIntentName} and child intent ${intentName}; -- returning default`);
		return getDefaultMaxDisconfirms();
	} catch (e) {
		console.log('%%%%ERROR: Error caught in getMaxDisconfirms(): ' + e);
		return getDefaultMaxDisconfirms();
	}
}



function handleFallback(fulfillment) {
	setDefaultResponseText(fulfillment);
	setEvent(fulfillment, 'TEST_EVENT');
}

function setEnabledOptions(fulfillment) {
	setDefaultResponseText(fulfillment);
	configureOptions(fulfillment, ['repeat','status','lookup']);
}

function getDefaultMaxNomatches() {
	if (getDefaults().max_nomatches != undefined && getDefaults().max_nomatches != null) {
		return getDefaults().max_nomatches;
	}
	return getDefaultMaxRetries();
}

function getDefaultMaxNoinputs() {
	if (getDefaults().max_noinputs != undefined && getDefaults().max_noinputs != null) {
		return getDefaults().max_noinputs;
	}
	return getDefaultMaxRetries();
}

function getDefaultMaxRetries() {
	return getDefaults().max_retries;
}

function getNumRetries(fulfillment, intentName) {
	console.log('*** In getNumRetries: intentName = ' + intentName);
	var c = {};
	if (isSimpleIntent(intentName)) {
		c = getIntentContext(fulfillment, intentName);
	} else {
		c = getParentContext(fulfillment);
	}
	if (!c) {
		return 0;
	}
	const retries = c.parameters[intentName + '-retries'] || 0;
	console.log('*** In getNumretries: retries = ' + retries + '; all parent_intent parameters: ' + JSON.stringify(c.parameters));
	return retries;
}

function getNumNoinputs(fulfillment, intentName) {
	console.log('*** In getNumNoinputs: intentName = ' + intentName);
	var c = {};
	if (isSimpleIntent(intentName)) {
		c = getIntentContext(fulfillment, intentName);
	} else {
		c = getParentContext(fulfillment);
	}
	if (!c) {
		return 0;
	}
	const retries = c.parameters[intentName + '-noinputs'] || 0;
	console.log('*** In getNumNoinputs: noinputs = ' + retries + '; all parent_intent parameters: ' + JSON.stringify(c.parameters));
	return retries;
}

function getNumNomatches(fulfillment, intentName) {
	console.log('*** In getNumNomatches: intentName = ' + intentName);
	var c = {};
	if (isSimpleIntent(intentName)) {
		c = getIntentContext(fulfillment, intentName);
	} else {
		c = getParentContext(fulfillment);
	}
	if (!c) {
		return 0;
	}
	const retries = c.parameters[intentName + '-nomatches'] || 0;
	console.log('*** In getNumNomatches: nomatches = ' + retries + '; all parent_intent parameters: ' + JSON.stringify(c.parameters));
	return retries;
}

function getNumDisconfirms(fulfillment, intentName) {
	console.log('*** In getNumDisconfirms: intentName = ' + intentName);
	var c = {};
	if (isSimpleIntent(intentName)) {
		c = getIntentContext(fulfillment, intentName);
	} else {
		c = getParentContext(fulfillment);
	}
	if (!c) {
		return 0;
	}
	const disconfirms = c.parameters[intentName + '-disconfirms'];
	console.log('*** In getNumDisconfirms: disconfirms = ' + disconfirms + '; all parent_intent parameters: ' + JSON.stringify(c.parameters));
	if (!disconfirms) {
		return 0;
	}
	return disconfirms;
}

function incrementRetriesCount(fulfillment, intentName) {
	var c = {};
	
	if (isSimpleIntent(intentName)) {
		c = getIntentContext(fulfillment, intentName);
	} else {
		c = getParentContext(fulfillment);
	}
	
	console.log('***incrementRetriesCount: context is: ' + JSON.stringify(c)); 
	if (!c) {
		// something is wrong here; we should always have a parent_intent context
		console.log('***WARNING! in incrementRetriesCount; parent_intent context does not exist');
		return;
	}
	var retries = c.parameters[intentName + '-retries'] || 0;
	c.parameters[intentName + '-retries'] = retries + 1;
	console.log('***incrementRetriesCount: after incrementing: context is: ' + JSON.stringify(c)); 
	if (!isSimpleIntent(intentName)) {
		setParentContext(fulfillment, c.parameters);
	} else {
		setIntentContext(fulfillment, intentName, c.parameters);
	}
	console.log('*** Exiting incrementRetriesCount; incremented retries; fulfillment = ' + JSON.stringify(fulfillment));
	
}

function incrementNoinputsCount(fulfillment, intentName) {
	var c = {};
	
	if (isSimpleIntent(intentName)) {
		c = getIntentContext(fulfillment, intentName);
	} else {
		c = getParentContext(fulfillment);
	}
	
	console.log('***incrementNoinputsCount: context is: ' + JSON.stringify(c)); 
	if (!c) {
		// something is wrong here; we should always have a parent_intent context
		console.log('***WARNING! in incrementNoinputsCount; parent_intent context does not exist');
		return;
	}
	var retries = c.parameters[intentName + '-noinputs'] || 0;
	c.parameters[intentName + '-noinputs'] = retries + 1;
	console.log('***incrementNoinputsCount: after incrementing: context is: ' + JSON.stringify(c)); 
	if (!isSimpleIntent(intentName)) {
		setParentContext(fulfillment, c.parameters);
	} else {
		setIntentContext(fulfillment, intentName, c.parameters);
	}
	console.log('*** Exiting incrementNoinputsCount; incremented retries; fulfillment = ' + JSON.stringify(fulfillment));
	
}

function incrementNomatchesCount(fulfillment, intentName) {
	var c = {};
	
	if (isSimpleIntent(intentName)) {
		c = getIntentContext(fulfillment, intentName);
	} else {
		c = getParentContext(fulfillment);
	}
	
	console.log('***incrementNomatchesCount: context is: ' + JSON.stringify(c)); 
	if (!c) {
		// something is wrong here; we should always have a parent_intent context
		console.log('***WARNING! in incrementNomatchesCount; parent_intent context does not exist');
		return;
	}
	var retries = c.parameters[intentName + '-nomatches'] || 0;
	c.parameters[intentName + '-nomatches'] = retries + 1;
	console.log('***incrementNomatchesCount: after incrementing: context is: ' + JSON.stringify(c)); 
	if (!isSimpleIntent(intentName)) {
		setParentContext(fulfillment, c.parameters);
	} else {
		setIntentContext(fulfillment, intentName, c.parameters);
	}
	console.log('*** Exiting incrementNomatchesCount; incremented retries; fulfillment = ' + JSON.stringify(fulfillment));
	
}

function expandParametersInPromptText(fulfillment, promptText, intentName) {
	if (!promptText) {
		return promptText;
	}
	var newPromptText = promptText;
	console.log('**** Entering expandParametersInPromptText: promptText = "' + promptText + '"');
	
	if (getUnconfirmedParameters(fulfillment, intentName)) {
		
		for (const [paramName, paramValue] of Object.entries(getUnconfirmedParameters(fulfillment, intentName))) {
			
		
			var pattern = "\${" + paramName + "}";
			var re = new RegExp(pattern, "g");
			newPromptText = newPromptText.replace(pattern, paramValue);	
		}
		console.log('**** in expandParametersInPromptText: updated promptText with unconfrimed param values: "' + newPromptText + '"');
	}
	
	var c = {};
	if (isSimpleIntent(intentName)) {
		c = getIntentContext(fulfillment, intentName);
	} else {
		c = getParentContext(fulfillment);
	}
	
	if (c && c.parameters && c.parameters.requiredParameters) {
		
		for (const [paramName, paramValue] of Object.entries(c.parameters.requiredParameters)) {
			
		
			var pattern = "\${" + paramName + "}";
			var re = new RegExp(pattern, "g");
			newPromptText = newPromptText.replace(pattern, paramValue);	
		}
		console.log('**** in expandParametersInPromptText: updated promptText with filled required params = "' + newPromptText + '"');
	}
	
	var pattern = '\\${global\..*?}';
	var re = new RegExp(pattern, "g");
	const m = newPromptText.match(re);
	console.log('**** in expandParametersInPromptText: match array: ' + JSON.stringify(m));
	if (m) {
		for (var i = 0; i < m.length; i++) {
		   var thisMatch = m[i];
		   var thisMatchOriginal = thisMatch;
		   var value = '';
			console.log('**** in expandParametersInPromptText: found global param: ' + thisMatch);
			pattern = '[${}]';
			re = new RegExp(pattern, 'g');
			thisMatch = thisMatch.replace(re, '');
			thisMatch = thisMatch.replace('global.', '');
			const path = 'global.parameters.' + thisMatch;
			var global = fulfillment.getContext('global');
			
			try {
				value = eval (path);
				console.log('*** in expandParametersInPromptText: evaluated ' + thisMatchOriginal + ' to ' + value);
			} catch (e) {
				console.log('*** in expandParametersInPromptText: could not eval ' + path + ': error = ' + e);
			}
			newPromptText = newPromptText.replace(thisMatchOriginal, value);	
		}
	}
	return newPromptText;
}

function setGlobalParameter(fulfillment, name, value) {
	const c = fulfillment.getContext('global');
	if (!c) {
		let data = {[name]:value};
		let params = {data:data};
		fulfillment.setOutputContext('global', 50, params);
		return;
	}
	if (!c.parameters) {
		const data = {[name]:value};
		c.parameters = {data:data};
	}
	if (c && c.parameters) {
		if (!c.parameters.data) {
			c.parameters.data = {};
		}
		c.parameters.data[name] = value;
	}
	fulfillment.setOutputContext('global', 50, c.parameters);
}

function getGlobalParameter(fulfillment, name) {
	const c = fulfillment.getContext('global');
	if (c && c.parameters && c.parameters.data) {
		return c.parameters.data[name];
	}
	return null;
}

function incrementDisconfirmCount(fulfillment, intentName) {
	var c = {};
	
	if (isSimpleIntent(intentName)) {
		c = getIntentContext(fulfillment, intentName);
	} else {
		c = getParentContext(fulfillment);
	}
	
	console.log('***incrementDisconfirmCount: context is: ' + JSON.stringify(c)); 
	if (!c) {
		// something is wrong here; we should always have a parent_intent context
		console.log('***WARNING! in incrementDisconfirmCount; parent_intent context does not exist');
		return;
	}
	var disconfirms = c.parameters[intentName + '-disconfirms'] || 0;
	c.parameters[intentName + '-disconfirms'] = disconfirms + 1;
	console.log('***incrementDisconfirmCount: after incrementing: context is: ' + JSON.stringify(c)); 
	if (!isSimpleIntent(intentName)) {
		setParentContext(fulfillment, c.parameters);
	} else {
		setIntentContext(fulfillment, intentName, c.parameters);
	}
	
}

function setParentIntentName(fulfillment, intentName) {
	console.log('**** Entering setParentIntentName; parent intentName is ' + intentName);
	const currentParentIntent = getParentIntentName(fulfillment);
	console.log('**** In setParentIntentName; current parent intentName: ' + currentParentIntent);
	if (currentParentIntent != intentName) {
		let params = {name:intentName, requiredParameters:{}};
		if (currentParentIntent && getParentContext(fulfillment)) {
			
			let parent = getParentContext(fulfillment);
			parent.parameters = params;
			setParentContext(fulfillment, parent.parameters);
			
		} else {
			setParentContext(fulfillment, params);
			console.log('**** In setParentIntentName; set parent intentName: ' + intentName);
		}
	}
}

function getParentIntentName(fulfillment) {
	console.log('**** Entering getParentIntentName; fulfillment = ' + JSON.stringify(fulfillment));
	const c = fulfillment.getContext('parent_intent');
	if (c && c.parameters) {
		console.log('**** In getParentIntentName:  returning ' + c.parameters.name);
		return c.parameters.name;
	}
	console.log('**** In getParentIntentName: parent_intent context does not exist; returning ""');
	return '';
}

function isContainerIntent(intentName) {
	return getContainerIntentConfig(intentName) != null;
}

function isSimpleIntent(intentName) {
	return getSimpleIntentConfig(intentName) != null;
}

function isGlobalIntent(intentName) {
	return getGlobalIntentNames().includes(intentName);
}

function getGlobalIntentNames() {
	if (getDefaults() && getDefaults().globals) {
		return getDefaults().globals;
	}
	return [];
}

function getDefaults() {
	if (dialogConfig && dialogConfig.defaults) {
		return dialogConfig.defaults;
	}
	throw new Error('no defaults exist');
}

function getRequiredParameterName(fulfillment, parentIntentName, intentName) {
	console.log('**** Entering getRequiredParameterName: intentName = ' + intentName + '; parentIntentName = ' + parentIntentName);
	if (isSimpleIntent(intentName)) {
		console.log('****  in getRequiredParameterName: handling simple intent ' + intentName);
		const intentConfig = getSimpleIntentConfig(intentName);
		if (intentConfig == null) {
			console.log('**** WARNING: in getRequiredParameterName: intentConfig for "' + intentName + '" does not exist');
			return '';
		}
		return intentConfig.paramName;
	}
	const intentConfig = getContainerIntentConfig(parentIntentName);
	if (intentConfig == null) {
		console.log('**** WARNING: in getRequiredParameterName: intentConfig for "' + parentIntentName + '" does not exist');
		return '';
	}
	const params = intentConfig.requiredParameters;
	for (var i = 0; i < params.length; i++) {
		const param = params[i];
		// console.log('${key}: ${value}');
		if (param.intentName == intentName) {
			return param.name;
		}
	}
	return '';
}

function getGlobals(fulfillment, parentIntentName, intentName) {
	console.log('**** Entering getGlobals: intentName = ' + intentName + '; parentIntentName = ' + parentIntentName);
	if (isContainerIntent(intentName)) {
		
		const intentConfig = getContainerIntentConfig(parentIntentName);
		if (intentConfig == null) {
			console.log('**** WARNING: in getGlobals: intentConfig for "' + parentIntentName + '" does not exist');
			return '';
		}
		const params = intentConfig.requiredParameters;
		for (var i = 0; i < params.length; i++) {
			const param = params[i];
			// console.log('${key}: ${value}');
			if (param.intentName == intentName) {
				return param.globals;
			}
		}
		return null;
	}
	
	if (isSimpleIntent(intentName)) {
		const intentConfig = getSimpleIntentConfig(intentName);
		return intentConfig.globals;
	}
	
	if (isDefaultIntentName(intentName)) {
		return getGlobalsFromPayload(fulfillment);
	}
	
	return null;
}

function retryIntent(fulfillment, intentName, promptMode, errorCounterMode) {
	if (errorCounterMode == 'noinput') {
		incrementNoinputsCount(fulfillment, intentName); 
	}
	
	if (errorCounterMode == 'nomatch') {
		incrementNomatchesCount(fulfillment, intentName); 
	}
	
	if (errorCounterMode == 'retry') {
		incrementRetriesCount(fulfillment, intentName); 
	}
	if (intentName == 'Confirm') {
		const intentToConfirm = getIntentToConfirm(fulfillment);
		triggerConfirmation(fulfillment, intentToConfirm, promptMode); // TODO
	} else if (intentName == 'NLU') {
		//handleNLU(fulfillment, 'retry');
		triggerIntent(fulfillment, 'NLU', {mode:promptMode}); // TODO
	} else {
		triggerIntent(fulfillment, intentName, {mode:promptMode}); // TODO
	}
	
}


function getPreviousIntentName(fulfillment) {
	return getGlobalParameter(fulfillment, 'previous_intent');
}


function getLastIntentName(fulfillment) {
	return getGlobalParameter(fulfillment, 'last_intent');
}

function setLastIntentName(fulfillment, intentDisplayName) {
	console.log(`*** setting last intent: ${intentDisplayName}`);
	setGlobalParameter(fulfillment, 'last_intent', intentDisplayName);
	console.log('*** done setting last intent; fulfillment: ' + JSON.stringify(fulfillment));
}

function setPreviousIntentName(fulfillment, intentDisplayName) {
	console.log(`*** setting previous intent: ${intentDisplayName}`);
	setGlobalParameter(fulfillment, 'previous_intent', intentDisplayName);
	console.log('*** done setting previous intent; fulfillment: ' + JSON.stringify(fulfillment));
}



function enableIntent(fulfillment, intentName) {
	console.log('*** Entering "enableIntent" for ' + intentName);
	fulfillment.setOutputContext(intentToContextName(intentName), 1, {});
}

function enableConfirmIntent(fulfillment, intentName) {
	console.log('*** Entering "enableConfirmIntent" for ' + intentName);
	fulfillment.setOutputContext(intentToContextName('Confirm'), 1, {intentName:intentName});
	
	enableGlobalIntents(fulfillment, intentName);
	enableOtherActiveIntents(fulfillment, intentName);
	
}

function getIntentToConfirm(fulfillment) {
	const c = fulfillment.getContext(intentToContextName('Confirm'));
	if (c) {
		return c.parameters.intentName;
	}
	return '';
}

function getCurrentNoInputCount(fulfillment) {
	const noInputCount = fulfillment._request.originalDetectIntentRequest.payload['Genesys-No-Input-Count'];
	return noInputCount;
}

function getIsNoInput(fulfillment) {
	const previousNoInputCount = getGlobalParameter(fulfillment, 'numNoInputs') || 0;
	console.log("*** In getIsNoInput: previousNoInputCount = " + previousNoInputCount);
	const noInputCount = getCurrentNoInputCount(fulfillment);
	console.log("*** In getIsNoInput: current NoInputCount = " + noInputCount);
	if (noInputCount > previousNoInputCount) {
		setGlobalParameter(fulfillment, 'numNoInputs', noInputCount);
		console.log("***** NOINPUT found; " + JSON.stringify(fulfillment));
		return true;
	}
	console.log("***** NOINPUT NOT found; ");
	return false;
}

function handleNoinput(fulfillment, parentIntent, intentName) {
	
	
	var maxNoinputs = getMaxNoinputs(fulfillment, parentIntentName, intentName);
	
	var numNoinputs = getNumNoinputs(fulfillment, intentName);
	console.log('*** Entering "handleNoInput"; intentName = ' + intentName + '; maxNoInputs = ' + maxNoinputs + '; numNoInputs = ' + numNoinputs);
	if ((numNoinputs + 1) < maxNoinputs) {
		console.log('*** In handleNoInput; numNoInputs is less than maxNoInputs; calling retryIntent' );
		retryIntent(fulfillment, intentName, 'noinput');
	} else {
		console.log('*** In handleNoInput; numRNoInputs is greater than maxNoInputs; about to get max_noinputs_next' );
		
		const nextIntent = getMaxNoinputsNextIntent(fulfillment, parentIntentName, intentName);
		console.log('*** In dhandleNoInput; max_noinputs_next = ' + nextIntent);
		if (nextIntent) {
			triggerIntent(fulfillment, nextIntent);
		} else {
			triggerIntent(fulfillment, getDefaultMaxNoinputsNextIntent(), {intentType:intentName});
		}
	}
}

function getMaxErrorsNextIntent(fulfillment, parentIntentName, intentName, errorCounterMode) {
	
	var maxErrorsNext = getMaxRetriesNextIntent(fulfillment, parentIntentName, intentName);
	if (errorCounterMode == 'noinput') {
		const maxNoinputsNext = getMaxNoinputsNextIntent(fulfillment, parentIntentName, intentName);
		if (maxNoinputsNext) {
			clearErrorCounters(fulfillment, parentIntentName, intentName);
			return maxNoinputsNext;
		} 		
	}
	
	if (errorCounterMode == 'nomatch') {
		const maxNomatchesNext = getMaxNomatchesNextIntent(fulfillment, parentIntentName, intentName);
		if (maxNomatchesNext) {
			clearErrorCounters(fulfillment, parentIntentName, intentName);
			return maxNomatchesNext;
		} 		
	}
	clearErrorCounters(fulfillment, parentIntentName, intentName);
	return maxErrorsNext;
}

function getCustomRetryHandler(fulfillment, intentName) {
	try {
		
		var intentConfig = {};
		if (isSimpleIntent(intentName)) {
			intentConfig = getSimpleIntentConfig(intentName);
			console.log (`++++ in getCustomRetryHandler:  for simple intent ${intentName}`);
			const handler =  intentConfig.retryHandler;
			if (handler) {
				console.log ('++++ in getCustomRetryHandler:  found handler. ');
			}
			return handler;
		} else {
			return null;
		}
	} catch (e) {
		return null;
	}
}

function customRetryHandler(fulfillment, intentName, promptMode, errorCounterMode, numErrors, maxErrors) {
	var handler = getCustomRetryHandler(fulfillment, intentName);
	if (handler) {
		return handler(fulfillment, intentName, promptMode, errorCounterMode, numErrors, maxErrors);
	}
	return false;
}
	
function default_fallback_intent(fulfillment, intent) {
	
	// get the name of the intent where the nomatch/noinput occurred
	var intentName = '';
	if (intent) {
		intentName = intent;
	} else {
		intentName = getLastIntentName(fulfillment);
	}
	
	console.log('*** Entering "default_fallback_intent"; last intent: ' + intentName + '; fulfillment = ' + JSON.stringify(fulfillment));
	const isNoInput = getIsNoInput(fulfillment);
	if (isNoInput) {
		console.log('*** Entering "default_fallback_intent" -- NOINPUT caught');
	}
	var parentIntentName = '';
	if (isSimpleIntent(intentName)) {
		parentIntentName = null;
	} else if (isContainerIntent(intentName)) {
		parentIntentName = getParentIntentName(fulfillment);
	} else if (intentName != 'Confirm') {
		handleDefaultIntentRetry(fulfillment, intentName);
		return;
	}
	var maxRetries = getMaxRetries(fulfillment, parentIntentName, intentName);
	
	var maxNoinputs = getMaxNoinputs(fulfillment, parentIntentName, intentName);
	
	var maxNomatches = getMaxNomatches(fulfillment, parentIntentName, intentName); 
	
	console.log('*** In "default_fallback_intent"; intentName = ' + intentName + '; maxRetries = ' + maxRetries + '; maxNoinputs = ' + maxNoinputs + '; maxNomatches = ' + maxNomatches);
	
	var errorCounterMode = 'retry';
	var promptMode = 'retry';
	var maxErrors = maxRetries;
	var numErrors = getNumRetries(fulfillment, intentName);
	if (isNoInput) {
		promptMode = 'noinput';
		if (isParameterDefinedForIntent(fulfillment, parentIntentName, intentName, 'max_noinputs') &&  isParameterDefinedForIntent(fulfillment, parentIntentName, intentName, 'max_nomatches')) {
			errorCounterMode = 'noinput';
			maxErrors = maxNoinputs;
			numErrors = getNumNoinputs(fulfillment, intentName); 
		}
	} else {
		promptMode = 'nomatch';
		if (isParameterDefinedForIntent(fulfillment, parentIntentName, intentName, 'max_noinputs') &&  isParameterDefinedForIntent(fulfillment, parentIntentName, intentName, 'max_nomatches'))  {
			errorCounterMode = 'nomatch';
			maxErrors = maxNomatches;
			numErrors = getNumNomatches(fulfillment, intentName); 
		}
	}
	
	
	console.log('*** In "default_fallback_intent"; intentName = ' + intentName + '; promptMode = ' + promptMode + '; errorCounterMode: ' + errorCounterMode + '; maxErrors = ' + maxErrors + '; numErrors = ' + numErrors);
	if (customRetryHandler(fulfillment, intentName, promptMode, errorCounterMode, numErrors, maxErrors)) {
		return;
	}
	if ((numErrors + 1) < maxErrors) {
		console.log('*** In default_fallback_intent; numErrors is less than maxErrors; calling retryIntent' );
		retryIntent(fulfillment, intentName, promptMode, errorCounterMode); // TODO
	} else {
		console.log('*** In default_fallback_intent; numErrors is greater than maxErrors; about to get max_errors_next' );
		
		const nextIntent = getMaxErrorsNextIntent(fulfillment, parentIntentName, intentName, errorCounterMode); 
		console.log('*** In default_fallback_intent; max_errors_next = ' + nextIntent);
		if (nextIntent) {
			triggerIntent(fulfillment, nextIntent);
		} else {
			triggerIntent(fulfillment, getDefaultMaxErrorsNextIntent(errorCounterMode), {intentType:intentName}); 
		}
	}
}

function getDefaultMaxErrorsNextIntent(errorCounterMode) {
	if (errorCounterMode == 'noinput') {
		return getDefaultMaxNoinputsNextIntent();
	}
	
	if (errorCounterMode == 'nomatch') {
		return getDefaultMaxNomatchesNextIntent();
	}
	return getDefaultMaxRetriesNextIntent();
}

const getIntentName = function(fulfillment) {
	 if (fulfillment && fulfillment._request  && fulfillment._request.queryResult && fulfillment._request.queryResult.intent) {
			
			
            return fulfillment._request.queryResult.intent.displayName;
			
	 }
	 return null;
}

function debug(message, enable) {
	if (enable) {
		console.log(message);
	}
}

function findCallableFunction(functionName) {
	var func = null;
	callableFuncions.forEach (f => {
		console.log('**** in findCallableFunction: f.name: ' + f.name);
		if (f.name == functionName) {
			func = f;
		}
	});
	
	
	
	return func;
}

function resolveNext(fulfillment, next) {
	if (next.startsWith('function::')) {
		var functionName = next.replace('function::','');
		console.log('*** in resolveNext; functionName is ' + functionName);
		var func = findCallableFunction(functionName);
		if (func instanceof Function) {
			console.log('*** in resolveNext; found function: ' + func.name);
			var newNext = func(fulfillment);
			console.log('*** in resolveNext; function evaluated to ' + newNext);
			return newNext;
		}
		throw new Error('function ' + functionName + ' not defined');
	}
	return next;
}

function noUnfilledParameters(fulfillment, intentDisplayName) {
	const params = fulfillment._request.queryResult.parameters;
	var result = true;
	if (params) {
		for (const [key, value] of Object.entries(params)) {
		  if (value == ''){
			  result = false;
		  }
		}
	}
	return result;
}

function noParametersToFill(fulfillment, intentDisplayName) {
	const params = fulfillment._request.queryResult.parameters;
	var result = true;
	if (params) {
		for (const [key, value] of Object.entries(params)) {
		  if (key != 'next'){
			  result = false;
		  }
		}
	}
	return result;
}

function handleDefaultIntent(fulfillment, intentDisplayName) {
	 console.log('*** In handleDefaultIntent: ' + intentDisplayName);
	 
	 if (!isGlobalIntent(intentDisplayName)) {
			saveCurrentIntentName(fulfillment, intentDisplayName);
		}
	 
	 var mode = 'initial'; //TODO: replace with a CONSTANT
	 if (isEventCaught(fulfillment)) {
		 
		 mode = getEventParameter(fulfillment, 'mode') || 'initial';
		 console.log('*** In handleDefaultIntent: event caught: ' + mode);
		 enableIntent(fulfillment, intentDisplayName);
	 } else if (!noUnfilledParameters(fulfillment, intentDisplayName)) {
		 // intent triggered, but no parameters were filled -- treat as a nomatch
		 return handleDefaultIntentRetry(fulfillment, intentDisplayName);
	 }
	 
	 if (mode == 'initial') {
		setDefaultResponseText(fulfillment);
		const payload = getPayload(fulfillment);
		if (payload) {
			setGlobalParameter(fulfillment, 'properties', payload);
		}
	 } else {
		 setDefaultRetryResponseText(fulfillment);
	 }
	 var next = getParameter(fulfillment, 'next');
	 
	 const lang = getParameter(fulfillment, 'language');
	 if (lang) {
		setLanguage(fulfillment, lang);
	 }
	 if (next && noUnfilledParameters(fulfillment, intentDisplayName)) {
		 next = resolveNext(fulfillment, next);
		 setGlobalParameter(fulfillment, intentDisplayName + '-retries', 0);
		 const params = fulfillment._request.queryResult.parameters;
		 if (params) {
			 setGlobalParameter(fulfillment, getRealIntentName(intentDisplayName), params);
		 }
		 
		 triggerIntent(fulfillment, next);
	 }
}

function getRealIntentName(intentName) {
	if (intentName && intentName.includes(' - ')) {
		// this is a follow up intent name, for example 'BaseMenu - option1'
		// the parent intent name will be before ' - '
		const parentIntentName = intentName.split(' - ')[0];
		intentName = parentIntentName;
	}
	return intentName;
}

function handleDefaultIntentRetry(fulfillment, intentName) {
	 console.log('*** In handleDefaultIntentRetry: ' + intentName);
	
	var nextIntent = intentName;
	var numErrors = getGlobalParameter(fulfillment, intentName+'-retries') || 1;
	var maxErrors = getMaxErrorsFromPayload(fulfillment);
	if (maxErrors == undefined || maxErrors == null || maxErrors == '') {
		maxErrors = getDefaultMaxRetries();
	}
	var params = {'mode':'retry'};
	if (numErrors == maxErrors || numErrors > maxErrors) {
		nextIntent = getMaxRetriesNextIntentFromPayload(fulfillment);
		if (nextIntent == undefined || nextIntent == null || nextIntent == '') {
			nextIntent = getDefaultMaxRetriesNextIntent();
		}
		params = {}
	} else {
		setGlobalParameter(fulfillment, intentName+'-retries', numErrors + 1);
	}
	triggerIntent(fulfillment, nextIntent, params);
					 
}

function getPayload(fulfillment){
	
	console.log('*** entering getPayload; fulfillment = ' + JSON.stringify(fulfillment));
	if (fulfillment && fulfillment._request && fulfillment._request.queryResult && fulfillment._request.queryResult.fulfillmentMessages && fulfillment._request.queryResult.fulfillmentMessages[1]) {
		
		var payload = fulfillment._request.queryResult.fulfillmentMessages[1].payload;
		if (payload) {
			return payload;
		}
		
		if (fulfillment._request.queryResult.fulfillmentMessages[2]) {
			payload = fulfillment._request.queryResult.fulfillmentMessages[2].payload;
			if (payload) {
				return payload;
			}
				
		}
			
	}
	return undefined;
}
	
function getMaxErrorsFromPayload(fulfillment){
	
	console.log('*** entering getMaxErrorsFromPayload; fulfillment = ' + JSON.stringify(fulfillment));
	
	const props = getGlobalParameter(fulfillment, 'properties');
	if (props) {
		return props.max_retries
	}
	return null;
	
}

function getGlobalsFromPayload(fulfillment){
	
	console.log('*** entering getMaxErrorsFromPayload; fulfillment = ' + JSON.stringify(fulfillment));
	
	const props = getGlobalParameter(fulfillment, 'properties');
	if (props) {
		return props.globals;
	}
	return null;
	
}

function getMaxRetriesNextIntentFromPayload(fulfillment){
	
	console.log('*** entering getMaxErrorsNextIntentFromPayload; fulfillment = ' + JSON.stringify(fulfillment));
	const props = getGlobalParameter(fulfillment, 'properties');
	if (props) {
		return props.max_retries_next;
	}
	return null;
}

const runStateMachine = function(fulfillment, response) {
    
    console.log('In runStateMachine: Dialogflow Request body: ' + JSON.stringify(fulfillment));

    
    try {
		
		
        if (fulfillment._request && fulfillment._request.queryResult && fulfillment._request.queryResult.intent) {
			
			logDebugInfoStart(fulfillment);
			if (isEventCaught(fulfillment) && (getEventParameter(fulfillment, 'mode') != 'retry') && (getEventParameter(fulfillment, 'mode') != 'nomatch') && (getEventParameter(fulfillment, 'mode') != 'noinput') && (getEventParameter(fulfillment, 'mode') != 'confirm') && (getEventParameter(fulfillment, 'mode') != 'disconfirm')) {
				setFulfillmentTextFromBuffer(fulfillment);
			}
            var intentDisplayName = getIntentName(fulfillment);
			
			if (intentDisplayName == 'default_welcome_intent') {						
				createDefaultsFromPayload(fulfillment);
			} else {
				setDefaultsFromPayload(fulfillment);
			}
			if (isEventCaught(fulfillment) && getEventName(fulfillment) == 'NLU') {
				console.log('*** fulfillment for NLU');
				const mode = getEventParameter(fulfillment, 'mode') || 'initial';
				intentDisplayName = 'NLU';
				handleNLU(fulfillment, mode);
			} else if (intentDisplayName == 'default_welcome_intent' && (isContainerIntent(intentDisplayName) || isSimpleIntent(intentDisplayName))) {
				
				handleWelcomeIntent(fulfillment);
            } else if (isContainerIntent(intentDisplayName)) {
				console.log('***fulfillment for top level intent: ' + intentDisplayName);
				handleContainerIntent(fulfillment, intentDisplayName);
			} else if (intentDisplayName == 'Confirm') {
				console.log('***fulfillment for "Confirm" intent: ' + intentDisplayName);
				handleConfirm(fulfillment);
				
			} else if (intentDisplayName == 'Default Fallback Intent') {
				console.log('***fulfillment for "Default Fallback Intent" intent: ' + intentDisplayName);
				default_fallback_intent(fulfillment);
			} else if (isSimpleIntent(intentDisplayName)) {
				console.log('***fulfillment for simple intent: ' + intentDisplayName);
				handleSimpleIntent(fulfillment, intentDisplayName);
			} else if (isChildIntent(intentDisplayName)){
				console.log('***fulfillment for follow-up intent: ' + intentDisplayName);
				handleIntent(fulfillment, intentDisplayName);
			} else {
				handleDefaultIntent(fulfillment, intentDisplayName);
			}
			if (!getBargeIn()) {
				setBargeIn(fulfillment, false);
			}
			setPreviousIntentName(fulfillment, getLastIntentName(fulfillment));
			setLastIntentName(fulfillment, intentDisplayName);
        }
        else
            throw new Error('Missing intent');

        console.log('Webhook Response: ' + JSON.stringify(fulfillment.getCompiledResponse()));
		logDebugInfoEnd(fulfillment);
        response.json(fulfillment.getCompiledResponse());
    }
    catch (e) {
        console.log(e);
        console.log(`%%%%ERROR: caught in runStatemachine(): ${e.message}`);
    }
}

module.exports = { config, initialize, setCallableFuncions, runStateMachine, getParentIntentName, getIntentName, getIntentContext, setEvent, setLanguage, getLanguage, setBargeIn, getBargeIn, getLastIntentName, setDialogConfig, getPreviousIntentName, triggerIntent, enableIntent, getFilledRequiredParameters, getFilledRequiredParameter, getParameter, setRequiredParameter, clearRequiredParameter, getUnconfirmedParameters, getUnconfirmedParameterValue, getGlobalParameter, setGlobalParameter, addContainerIntentToConfig, addSimpleIntentToConfig, addDefaultPropertiesToConfig };